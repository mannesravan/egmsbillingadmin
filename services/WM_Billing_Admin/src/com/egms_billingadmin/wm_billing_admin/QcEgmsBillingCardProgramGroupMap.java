/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * QcEgmsBillingCardProgramGroupMap generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`QC_EGMS_BILLING_CARD_PROGRAM_GROUP_MAP`", uniqueConstraints = {
            @UniqueConstraint(name = "`UQ__QC_EGMS___84BE6B14475F3D79`", columnNames = {"`CARDPROGRAMGROUPID`"})})
public class QcEgmsBillingCardProgramGroupMap implements Serializable {

    private Long cardprogrambillinggroupid;
    private long cardprogramgroupid;
    private long billinggrouppkid;
    private long billinggroupid;
    private Boolean isenabled;
    private LocalDateTime creationdate;
    private Long createdby;
    private LocalDateTime lastmodifieddate;
    private Long lastmodifiedby;
    private QcEgmsBillingGroups qcEgmsBillingGroups;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`CARDPROGRAMBILLINGGROUPID`", nullable = false, scale = 0, precision = 19)
    public Long getCardprogrambillinggroupid() {
        return this.cardprogrambillinggroupid;
    }

    public void setCardprogrambillinggroupid(Long cardprogrambillinggroupid) {
        this.cardprogrambillinggroupid = cardprogrambillinggroupid;
    }

    @Column(name = "`CARDPROGRAMGROUPID`", nullable = false, scale = 0, precision = 19)
    public long getCardprogramgroupid() {
        return this.cardprogramgroupid;
    }

    public void setCardprogramgroupid(long cardprogramgroupid) {
        this.cardprogramgroupid = cardprogramgroupid;
    }

    @Column(name = "`BILLINGGROUPPKID`", nullable = false, scale = 0, precision = 19)
    public long getBillinggrouppkid() {
        return this.billinggrouppkid;
    }

    public void setBillinggrouppkid(long billinggrouppkid) {
        this.billinggrouppkid = billinggrouppkid;
    }

    @Column(name = "`BILLINGGROUPID`", nullable = false, scale = 0, precision = 19)
    public long getBillinggroupid() {
        return this.billinggroupid;
    }

    public void setBillinggroupid(long billinggroupid) {
        this.billinggroupid = billinggroupid;
    }

    @Column(name = "`ISENABLED`", nullable = true)
    public Boolean getIsenabled() {
        return this.isenabled;
    }

    public void setIsenabled(Boolean isenabled) {
        this.isenabled = isenabled;
    }

    @Column(name = "`CREATIONDATE`", nullable = true)
    public LocalDateTime getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(LocalDateTime creationdate) {
        this.creationdate = creationdate;
    }

    @Column(name = "`CREATEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(Long createdby) {
        this.createdby = createdby;
    }

    @Column(name = "`LASTMODIFIEDDATE`", nullable = true)
    public LocalDateTime getLastmodifieddate() {
        return this.lastmodifieddate;
    }

    public void setLastmodifieddate(LocalDateTime lastmodifieddate) {
        this.lastmodifieddate = lastmodifieddate;
    }

    @Column(name = "`LASTMODIFIEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getLastmodifiedby() {
        return this.lastmodifiedby;
    }

    public void setLastmodifiedby(Long lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`BILLINGGROUPPKID`", referencedColumnName = "`BILLINGGROUPPKID`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_QC_EGMS_BILLING_CARD_PROGRAM_GROUP_QC_EGMS_BILLING_GROUPS`"))
    @Fetch(FetchMode.JOIN)
    public QcEgmsBillingGroups getQcEgmsBillingGroups() {
        return this.qcEgmsBillingGroups;
    }

    public void setQcEgmsBillingGroups(QcEgmsBillingGroups qcEgmsBillingGroups) {
        if(qcEgmsBillingGroups != null) {
            this.billinggrouppkid = qcEgmsBillingGroups.getBillinggrouppkid();
        }

        this.qcEgmsBillingGroups = qcEgmsBillingGroups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QcEgmsBillingCardProgramGroupMap)) return false;
        final QcEgmsBillingCardProgramGroupMap qcEgmsBillingCardProgramGroupMap = (QcEgmsBillingCardProgramGroupMap) o;
        return Objects.equals(getCardprogrambillinggroupid(), qcEgmsBillingCardProgramGroupMap.getCardprogrambillinggroupid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCardprogrambillinggroupid());
    }
}