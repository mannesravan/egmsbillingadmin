/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * QcEgmsBillingInvoiceComputedValues generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`QC_EGMS_BILLING_INVOICE_COMPUTED_VALUES`")
public class QcEgmsBillingInvoiceComputedValues implements Serializable {

    private Long invoicecomputedvalueid;
    private long invoiceid;
    private long paramid;
    private String paramvalue;
    private LocalDateTime creationdate;
    private Long createdby;
    private LocalDateTime lastmodifieddate;
    private Long lastmodifiedby;
    private QcEgmsBillingInvoices qcEgmsBillingInvoices;
    private QcEgmsBillingComputedParamMaster qcEgmsBillingComputedParamMaster;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`INVOICECOMPUTEDVALUEID`", nullable = false, scale = 0, precision = 19)
    public Long getInvoicecomputedvalueid() {
        return this.invoicecomputedvalueid;
    }

    public void setInvoicecomputedvalueid(Long invoicecomputedvalueid) {
        this.invoicecomputedvalueid = invoicecomputedvalueid;
    }

    @Column(name = "`INVOICEID`", nullable = false, scale = 0, precision = 19)
    public long getInvoiceid() {
        return this.invoiceid;
    }

    public void setInvoiceid(long invoiceid) {
        this.invoiceid = invoiceid;
    }

    @Column(name = "`PARAMID`", nullable = false, scale = 0, precision = 19)
    public long getParamid() {
        return this.paramid;
    }

    public void setParamid(long paramid) {
        this.paramid = paramid;
    }

    @Column(name = "`PARAMVALUE`", nullable = true, length = 1000)
    public String getParamvalue() {
        return this.paramvalue;
    }

    public void setParamvalue(String paramvalue) {
        this.paramvalue = paramvalue;
    }

    @Column(name = "`CREATIONDATE`", nullable = true)
    public LocalDateTime getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(LocalDateTime creationdate) {
        this.creationdate = creationdate;
    }

    @Column(name = "`CREATEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(Long createdby) {
        this.createdby = createdby;
    }

    @Column(name = "`LASTMODIFIEDDATE`", nullable = true)
    public LocalDateTime getLastmodifieddate() {
        return this.lastmodifieddate;
    }

    public void setLastmodifieddate(LocalDateTime lastmodifieddate) {
        this.lastmodifieddate = lastmodifieddate;
    }

    @Column(name = "`LASTMODIFIEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getLastmodifiedby() {
        return this.lastmodifiedby;
    }

    public void setLastmodifiedby(Long lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`INVOICEID`", referencedColumnName = "`INVOICEID`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_QC_EGMS_BILLING_COMPUTED_VALUES_QC_EGMS_BILLING_INVOICES`"))
    @Fetch(FetchMode.JOIN)
    public QcEgmsBillingInvoices getQcEgmsBillingInvoices() {
        return this.qcEgmsBillingInvoices;
    }

    public void setQcEgmsBillingInvoices(QcEgmsBillingInvoices qcEgmsBillingInvoices) {
        if(qcEgmsBillingInvoices != null) {
            this.invoiceid = qcEgmsBillingInvoices.getInvoiceid();
        }

        this.qcEgmsBillingInvoices = qcEgmsBillingInvoices;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`PARAMID`", referencedColumnName = "`PARAMID`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_QC_EGMS_BILLING_INVOICE_COMPUTED_VALUES_QC_EGMS_BILLING_COMPUTED_PARAM_MASTER`"))
    @Fetch(FetchMode.JOIN)
    public QcEgmsBillingComputedParamMaster getQcEgmsBillingComputedParamMaster() {
        return this.qcEgmsBillingComputedParamMaster;
    }

    public void setQcEgmsBillingComputedParamMaster(QcEgmsBillingComputedParamMaster qcEgmsBillingComputedParamMaster) {
        if(qcEgmsBillingComputedParamMaster != null) {
            this.paramid = qcEgmsBillingComputedParamMaster.getParamid();
        }

        this.qcEgmsBillingComputedParamMaster = qcEgmsBillingComputedParamMaster;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QcEgmsBillingInvoiceComputedValues)) return false;
        final QcEgmsBillingInvoiceComputedValues qcEgmsBillingInvoiceComputedValues = (QcEgmsBillingInvoiceComputedValues) o;
        return Objects.equals(getInvoicecomputedvalueid(), qcEgmsBillingInvoiceComputedValues.getInvoicecomputedvalueid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInvoicecomputedvalueid());
    }
}