/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * QcEgmsBillingParamMaster generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`QC_EGMS_BILLING_PARAM_MASTER`", uniqueConstraints = {
            @UniqueConstraint(name = "`UQ__QC_EGMS___8416C44EF8B6DB47`", columnNames = {"`PARAMNAME`"})})
public class QcEgmsBillingParamMaster implements Serializable {

    private Long paramid;
    private String paramname;
    private String paramdisplayname;
    private Long paramdatatypeid;
    private LocalDateTime creationdate;
    private Long createdby;
    private LocalDateTime lastmodifieddate;
    private Long lastmodifiedby;
    private QcEgmsBillingParamDataTypeLk qcEgmsBillingParamDataTypeLk;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`PARAMID`", nullable = false, scale = 0, precision = 19)
    public Long getParamid() {
        return this.paramid;
    }

    public void setParamid(Long paramid) {
        this.paramid = paramid;
    }

    @Column(name = "`PARAMNAME`", nullable = false, length = 64)
    public String getParamname() {
        return this.paramname;
    }

    public void setParamname(String paramname) {
        this.paramname = paramname;
    }

    @Column(name = "`PARAMDISPLAYNAME`", nullable = true, length = 64)
    public String getParamdisplayname() {
        return this.paramdisplayname;
    }

    public void setParamdisplayname(String paramdisplayname) {
        this.paramdisplayname = paramdisplayname;
    }

    @Column(name = "`PARAMDATATYPEID`", nullable = true, scale = 0, precision = 19)
    public Long getParamdatatypeid() {
        return this.paramdatatypeid;
    }

    public void setParamdatatypeid(Long paramdatatypeid) {
        this.paramdatatypeid = paramdatatypeid;
    }

    @Column(name = "`CREATIONDATE`", nullable = true)
    public LocalDateTime getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(LocalDateTime creationdate) {
        this.creationdate = creationdate;
    }

    @Column(name = "`CREATEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(Long createdby) {
        this.createdby = createdby;
    }

    @Column(name = "`LASTMODIFIEDDATE`", nullable = true)
    public LocalDateTime getLastmodifieddate() {
        return this.lastmodifieddate;
    }

    public void setLastmodifieddate(LocalDateTime lastmodifieddate) {
        this.lastmodifieddate = lastmodifieddate;
    }

    @Column(name = "`LASTMODIFIEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getLastmodifiedby() {
        return this.lastmodifiedby;
    }

    public void setLastmodifiedby(Long lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`PARAMDATATYPEID`", referencedColumnName = "`PARAMDATATYPEID`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_QC_EGMS_BILLING_PARAM_MASTER_DATA_TYPE`"))
    @Fetch(FetchMode.JOIN)
    public QcEgmsBillingParamDataTypeLk getQcEgmsBillingParamDataTypeLk() {
        return this.qcEgmsBillingParamDataTypeLk;
    }

    public void setQcEgmsBillingParamDataTypeLk(QcEgmsBillingParamDataTypeLk qcEgmsBillingParamDataTypeLk) {
        if(qcEgmsBillingParamDataTypeLk != null) {
            this.paramdatatypeid = qcEgmsBillingParamDataTypeLk.getParamdatatypeid();
        }

        this.qcEgmsBillingParamDataTypeLk = qcEgmsBillingParamDataTypeLk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QcEgmsBillingParamMaster)) return false;
        final QcEgmsBillingParamMaster qcEgmsBillingParamMaster = (QcEgmsBillingParamMaster) o;
        return Objects.equals(getParamid(), qcEgmsBillingParamMaster.getParamid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getParamid());
    }
}