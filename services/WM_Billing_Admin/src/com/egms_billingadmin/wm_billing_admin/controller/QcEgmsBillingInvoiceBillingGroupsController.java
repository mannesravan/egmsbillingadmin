/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.commons.wrapper.StringWrapper;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.manager.ExportedFileManager;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.runtime.security.xss.XssDisable;
import com.wavemaker.tools.api.core.annotations.MapTo;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingInvoiceBillingGroups;
import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingInvoiceCardprogramMap;
import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingInvoiceConfigValues;
import com.egms_billingadmin.wm_billing_admin.service.QcEgmsBillingInvoiceBillingGroupsService;


/**
 * Controller object for domain model class QcEgmsBillingInvoiceBillingGroups.
 * @see QcEgmsBillingInvoiceBillingGroups
 */
@RestController("WM_Billing_Admin.QcEgmsBillingInvoiceBillingGroupsController")
@Api(value = "QcEgmsBillingInvoiceBillingGroupsController", description = "Exposes APIs to work with QcEgmsBillingInvoiceBillingGroups resource.")
@RequestMapping("/WM_Billing_Admin/QcEgmsBillingInvoiceBillingGroups")
public class QcEgmsBillingInvoiceBillingGroupsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(QcEgmsBillingInvoiceBillingGroupsController.class);

    @Autowired
	@Qualifier("WM_Billing_Admin.QcEgmsBillingInvoiceBillingGroupsService")
	private QcEgmsBillingInvoiceBillingGroupsService qcEgmsBillingInvoiceBillingGroupsService;

	@Autowired
	private ExportedFileManager exportedFileManager;

	@ApiOperation(value = "Creates a new QcEgmsBillingInvoiceBillingGroups instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public QcEgmsBillingInvoiceBillingGroups createQcEgmsBillingInvoiceBillingGroups(@RequestBody QcEgmsBillingInvoiceBillingGroups qcEgmsBillingInvoiceBillingGroups) {
		LOGGER.debug("Create QcEgmsBillingInvoiceBillingGroups with information: {}" , qcEgmsBillingInvoiceBillingGroups);

		qcEgmsBillingInvoiceBillingGroups = qcEgmsBillingInvoiceBillingGroupsService.create(qcEgmsBillingInvoiceBillingGroups);
		LOGGER.debug("Created QcEgmsBillingInvoiceBillingGroups with information: {}" , qcEgmsBillingInvoiceBillingGroups);

	    return qcEgmsBillingInvoiceBillingGroups;
	}

    @ApiOperation(value = "Returns the QcEgmsBillingInvoiceBillingGroups instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public QcEgmsBillingInvoiceBillingGroups getQcEgmsBillingInvoiceBillingGroups(@PathVariable("id") Long id) {
        LOGGER.debug("Getting QcEgmsBillingInvoiceBillingGroups with id: {}" , id);

        QcEgmsBillingInvoiceBillingGroups foundQcEgmsBillingInvoiceBillingGroups = qcEgmsBillingInvoiceBillingGroupsService.getById(id);
        LOGGER.debug("QcEgmsBillingInvoiceBillingGroups details with id: {}" , foundQcEgmsBillingInvoiceBillingGroups);

        return foundQcEgmsBillingInvoiceBillingGroups;
    }

    @ApiOperation(value = "Updates the QcEgmsBillingInvoiceBillingGroups instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public QcEgmsBillingInvoiceBillingGroups editQcEgmsBillingInvoiceBillingGroups(@PathVariable("id") Long id, @RequestBody QcEgmsBillingInvoiceBillingGroups qcEgmsBillingInvoiceBillingGroups) {
        LOGGER.debug("Editing QcEgmsBillingInvoiceBillingGroups with id: {}" , qcEgmsBillingInvoiceBillingGroups.getInvoicebillinggroupid());

        qcEgmsBillingInvoiceBillingGroups.setInvoicebillinggroupid(id);
        qcEgmsBillingInvoiceBillingGroups = qcEgmsBillingInvoiceBillingGroupsService.update(qcEgmsBillingInvoiceBillingGroups);
        LOGGER.debug("QcEgmsBillingInvoiceBillingGroups details with id: {}" , qcEgmsBillingInvoiceBillingGroups);

        return qcEgmsBillingInvoiceBillingGroups;
    }
    
    @ApiOperation(value = "Partially updates the QcEgmsBillingInvoiceBillingGroups instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PATCH)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public QcEgmsBillingInvoiceBillingGroups patchQcEgmsBillingInvoiceBillingGroups(@PathVariable("id") Long id, @RequestBody @MapTo(QcEgmsBillingInvoiceBillingGroups.class) Map<String, Object> qcEgmsBillingInvoiceBillingGroupsPatch) {
        LOGGER.debug("Partially updating QcEgmsBillingInvoiceBillingGroups with id: {}" , id);

        QcEgmsBillingInvoiceBillingGroups qcEgmsBillingInvoiceBillingGroups = qcEgmsBillingInvoiceBillingGroupsService.partialUpdate(id, qcEgmsBillingInvoiceBillingGroupsPatch);
        LOGGER.debug("QcEgmsBillingInvoiceBillingGroups details after partial update: {}" , qcEgmsBillingInvoiceBillingGroups);

        return qcEgmsBillingInvoiceBillingGroups;
    }

    @ApiOperation(value = "Deletes the QcEgmsBillingInvoiceBillingGroups instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteQcEgmsBillingInvoiceBillingGroups(@PathVariable("id") Long id) {
        LOGGER.debug("Deleting QcEgmsBillingInvoiceBillingGroups with id: {}" , id);

        QcEgmsBillingInvoiceBillingGroups deletedQcEgmsBillingInvoiceBillingGroups = qcEgmsBillingInvoiceBillingGroupsService.delete(id);

        return deletedQcEgmsBillingInvoiceBillingGroups != null;
    }

    /**
     * @deprecated Use {@link #findQcEgmsBillingInvoiceBillingGroups(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of QcEgmsBillingInvoiceBillingGroups instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public Page<QcEgmsBillingInvoiceBillingGroups> searchQcEgmsBillingInvoiceBillingGroupsByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering QcEgmsBillingInvoiceBillingGroups list by query filter:{}", (Object) queryFilters);
        return qcEgmsBillingInvoiceBillingGroupsService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of QcEgmsBillingInvoiceBillingGroups instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<QcEgmsBillingInvoiceBillingGroups> findQcEgmsBillingInvoiceBillingGroups(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering QcEgmsBillingInvoiceBillingGroups list by filter:", query);
        return qcEgmsBillingInvoiceBillingGroupsService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of QcEgmsBillingInvoiceBillingGroups instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public Page<QcEgmsBillingInvoiceBillingGroups> filterQcEgmsBillingInvoiceBillingGroups(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering QcEgmsBillingInvoiceBillingGroups list by filter", query);
        return qcEgmsBillingInvoiceBillingGroupsService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public Downloadable exportQcEgmsBillingInvoiceBillingGroups(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return qcEgmsBillingInvoiceBillingGroupsService.export(exportType, query, pageable);
    }

    @ApiOperation(value = "Returns a URL to download a file for the data matching the optional query (q) request param and the required fields provided in the Export Options.") 
    @RequestMapping(value = "/export", method = {RequestMethod.POST}, consumes = "application/json")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public StringWrapper exportQcEgmsBillingInvoiceBillingGroupsAndGetURL(@RequestBody DataExportOptions exportOptions, Pageable pageable) {
        String exportedFileName = exportOptions.getFileName();
        if(exportedFileName == null || exportedFileName.isEmpty()) {
            exportedFileName = QcEgmsBillingInvoiceBillingGroups.class.getSimpleName();
        }
        exportedFileName += exportOptions.getExportType().getExtension();
        String exportedUrl = exportedFileManager.registerAndGetURL(exportedFileName, outputStream -> qcEgmsBillingInvoiceBillingGroupsService.export(exportOptions, pageable, outputStream));
        return new StringWrapper(exportedUrl);
    }

	@ApiOperation(value = "Returns the total count of QcEgmsBillingInvoiceBillingGroups instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	@XssDisable
	public Long countQcEgmsBillingInvoiceBillingGroups( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting QcEgmsBillingInvoiceBillingGroups");
		return qcEgmsBillingInvoiceBillingGroupsService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	@XssDisable
	public Page<Map<String, Object>> getQcEgmsBillingInvoiceBillingGroupsAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return qcEgmsBillingInvoiceBillingGroupsService.getAggregatedValues(aggregationInfo, pageable);
    }

    @RequestMapping(value="/{id:.+}/qcEgmsBillingInvoiceCardprogramMaps", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the qcEgmsBillingInvoiceCardprogramMaps instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<QcEgmsBillingInvoiceCardprogramMap> findAssociatedQcEgmsBillingInvoiceCardprogramMaps(@PathVariable("id") Long id, Pageable pageable) {

        LOGGER.debug("Fetching all associated qcEgmsBillingInvoiceCardprogramMaps");
        return qcEgmsBillingInvoiceBillingGroupsService.findAssociatedQcEgmsBillingInvoiceCardprogramMaps(id, pageable);
    }

    @RequestMapping(value="/{id:.+}/qcEgmsBillingInvoiceConfigValueses", method=RequestMethod.GET)
    @ApiOperation(value = "Gets the qcEgmsBillingInvoiceConfigValueses instance associated with the given id.")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<QcEgmsBillingInvoiceConfigValues> findAssociatedQcEgmsBillingInvoiceConfigValueses(@PathVariable("id") Long id, Pageable pageable) {

        LOGGER.debug("Fetching all associated qcEgmsBillingInvoiceConfigValueses");
        return qcEgmsBillingInvoiceBillingGroupsService.findAssociatedQcEgmsBillingInvoiceConfigValueses(id, pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service QcEgmsBillingInvoiceBillingGroupsService instance
	 */
	protected void setQcEgmsBillingInvoiceBillingGroupsService(QcEgmsBillingInvoiceBillingGroupsService service) {
		this.qcEgmsBillingInvoiceBillingGroupsService = service;
	}

}