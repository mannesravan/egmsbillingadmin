/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.commons.wrapper.StringWrapper;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.manager.ExportedFileManager;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.runtime.security.xss.XssDisable;
import com.wavemaker.tools.api.core.annotations.MapTo;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingInvoiceConfigValues;
import com.egms_billingadmin.wm_billing_admin.service.QcEgmsBillingInvoiceConfigValuesService;


/**
 * Controller object for domain model class QcEgmsBillingInvoiceConfigValues.
 * @see QcEgmsBillingInvoiceConfigValues
 */
@RestController("WM_Billing_Admin.QcEgmsBillingInvoiceConfigValuesController")
@Api(value = "QcEgmsBillingInvoiceConfigValuesController", description = "Exposes APIs to work with QcEgmsBillingInvoiceConfigValues resource.")
@RequestMapping("/WM_Billing_Admin/QcEgmsBillingInvoiceConfigValues")
public class QcEgmsBillingInvoiceConfigValuesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(QcEgmsBillingInvoiceConfigValuesController.class);

    @Autowired
	@Qualifier("WM_Billing_Admin.QcEgmsBillingInvoiceConfigValuesService")
	private QcEgmsBillingInvoiceConfigValuesService qcEgmsBillingInvoiceConfigValuesService;

	@Autowired
	private ExportedFileManager exportedFileManager;

	@ApiOperation(value = "Creates a new QcEgmsBillingInvoiceConfigValues instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public QcEgmsBillingInvoiceConfigValues createQcEgmsBillingInvoiceConfigValues(@RequestBody QcEgmsBillingInvoiceConfigValues qcEgmsBillingInvoiceConfigValues) {
		LOGGER.debug("Create QcEgmsBillingInvoiceConfigValues with information: {}" , qcEgmsBillingInvoiceConfigValues);

		qcEgmsBillingInvoiceConfigValues = qcEgmsBillingInvoiceConfigValuesService.create(qcEgmsBillingInvoiceConfigValues);
		LOGGER.debug("Created QcEgmsBillingInvoiceConfigValues with information: {}" , qcEgmsBillingInvoiceConfigValues);

	    return qcEgmsBillingInvoiceConfigValues;
	}

    @ApiOperation(value = "Returns the QcEgmsBillingInvoiceConfigValues instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public QcEgmsBillingInvoiceConfigValues getQcEgmsBillingInvoiceConfigValues(@PathVariable("id") Long id) {
        LOGGER.debug("Getting QcEgmsBillingInvoiceConfigValues with id: {}" , id);

        QcEgmsBillingInvoiceConfigValues foundQcEgmsBillingInvoiceConfigValues = qcEgmsBillingInvoiceConfigValuesService.getById(id);
        LOGGER.debug("QcEgmsBillingInvoiceConfigValues details with id: {}" , foundQcEgmsBillingInvoiceConfigValues);

        return foundQcEgmsBillingInvoiceConfigValues;
    }

    @ApiOperation(value = "Updates the QcEgmsBillingInvoiceConfigValues instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public QcEgmsBillingInvoiceConfigValues editQcEgmsBillingInvoiceConfigValues(@PathVariable("id") Long id, @RequestBody QcEgmsBillingInvoiceConfigValues qcEgmsBillingInvoiceConfigValues) {
        LOGGER.debug("Editing QcEgmsBillingInvoiceConfigValues with id: {}" , qcEgmsBillingInvoiceConfigValues.getInvoiceparamid());

        qcEgmsBillingInvoiceConfigValues.setInvoiceparamid(id);
        qcEgmsBillingInvoiceConfigValues = qcEgmsBillingInvoiceConfigValuesService.update(qcEgmsBillingInvoiceConfigValues);
        LOGGER.debug("QcEgmsBillingInvoiceConfigValues details with id: {}" , qcEgmsBillingInvoiceConfigValues);

        return qcEgmsBillingInvoiceConfigValues;
    }
    
    @ApiOperation(value = "Partially updates the QcEgmsBillingInvoiceConfigValues instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.PATCH)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public QcEgmsBillingInvoiceConfigValues patchQcEgmsBillingInvoiceConfigValues(@PathVariable("id") Long id, @RequestBody @MapTo(QcEgmsBillingInvoiceConfigValues.class) Map<String, Object> qcEgmsBillingInvoiceConfigValuesPatch) {
        LOGGER.debug("Partially updating QcEgmsBillingInvoiceConfigValues with id: {}" , id);

        QcEgmsBillingInvoiceConfigValues qcEgmsBillingInvoiceConfigValues = qcEgmsBillingInvoiceConfigValuesService.partialUpdate(id, qcEgmsBillingInvoiceConfigValuesPatch);
        LOGGER.debug("QcEgmsBillingInvoiceConfigValues details after partial update: {}" , qcEgmsBillingInvoiceConfigValues);

        return qcEgmsBillingInvoiceConfigValues;
    }

    @ApiOperation(value = "Deletes the QcEgmsBillingInvoiceConfigValues instance associated with the given id.")
    @RequestMapping(value = "/{id:.+}", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteQcEgmsBillingInvoiceConfigValues(@PathVariable("id") Long id) {
        LOGGER.debug("Deleting QcEgmsBillingInvoiceConfigValues with id: {}" , id);

        QcEgmsBillingInvoiceConfigValues deletedQcEgmsBillingInvoiceConfigValues = qcEgmsBillingInvoiceConfigValuesService.delete(id);

        return deletedQcEgmsBillingInvoiceConfigValues != null;
    }

    /**
     * @deprecated Use {@link #findQcEgmsBillingInvoiceConfigValues(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of QcEgmsBillingInvoiceConfigValues instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public Page<QcEgmsBillingInvoiceConfigValues> searchQcEgmsBillingInvoiceConfigValuesByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering QcEgmsBillingInvoiceConfigValues list by query filter:{}", (Object) queryFilters);
        return qcEgmsBillingInvoiceConfigValuesService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of QcEgmsBillingInvoiceConfigValues instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<QcEgmsBillingInvoiceConfigValues> findQcEgmsBillingInvoiceConfigValues(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering QcEgmsBillingInvoiceConfigValues list by filter:", query);
        return qcEgmsBillingInvoiceConfigValuesService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of QcEgmsBillingInvoiceConfigValues instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public Page<QcEgmsBillingInvoiceConfigValues> filterQcEgmsBillingInvoiceConfigValues(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering QcEgmsBillingInvoiceConfigValues list by filter", query);
        return qcEgmsBillingInvoiceConfigValuesService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public Downloadable exportQcEgmsBillingInvoiceConfigValues(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return qcEgmsBillingInvoiceConfigValuesService.export(exportType, query, pageable);
    }

    @ApiOperation(value = "Returns a URL to download a file for the data matching the optional query (q) request param and the required fields provided in the Export Options.") 
    @RequestMapping(value = "/export", method = {RequestMethod.POST}, consumes = "application/json")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @XssDisable
    public StringWrapper exportQcEgmsBillingInvoiceConfigValuesAndGetURL(@RequestBody DataExportOptions exportOptions, Pageable pageable) {
        String exportedFileName = exportOptions.getFileName();
        if(exportedFileName == null || exportedFileName.isEmpty()) {
            exportedFileName = QcEgmsBillingInvoiceConfigValues.class.getSimpleName();
        }
        exportedFileName += exportOptions.getExportType().getExtension();
        String exportedUrl = exportedFileManager.registerAndGetURL(exportedFileName, outputStream -> qcEgmsBillingInvoiceConfigValuesService.export(exportOptions, pageable, outputStream));
        return new StringWrapper(exportedUrl);
    }

	@ApiOperation(value = "Returns the total count of QcEgmsBillingInvoiceConfigValues instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	@XssDisable
	public Long countQcEgmsBillingInvoiceConfigValues( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting QcEgmsBillingInvoiceConfigValues");
		return qcEgmsBillingInvoiceConfigValuesService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	@XssDisable
	public Page<Map<String, Object>> getQcEgmsBillingInvoiceConfigValuesAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return qcEgmsBillingInvoiceConfigValuesService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service QcEgmsBillingInvoiceConfigValuesService instance
	 */
	protected void setQcEgmsBillingInvoiceConfigValuesService(QcEgmsBillingInvoiceConfigValuesService service) {
		this.qcEgmsBillingInvoiceConfigValuesService = service;
	}

}