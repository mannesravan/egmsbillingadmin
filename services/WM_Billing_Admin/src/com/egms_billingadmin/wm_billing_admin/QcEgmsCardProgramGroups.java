/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * QcEgmsCardProgramGroups generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`QC_EGMS_CARD_PROGRAM_GROUPS`", uniqueConstraints = {
            @UniqueConstraint(name = "`UNIQUE_QC_EGMS_CARD_PROGRAM_GROUPS`", columnNames = {"`CARDPROGRAMDESC`", "`ISSUERID`"})})
public class QcEgmsCardProgramGroups implements Serializable {

    private Long cardprogramgroupid;
    private String cardprogramdesc;
    private long issuerid;
    private LocalDateTime launchdate;
    private LocalDateTime expirydate;
    private long cardprogramgroupstatusid;
    private Long cardtypeid;
    private String binoriincode;
    private String merchantprefix;
    private Boolean autogeneratedcardenabled;
    private Boolean autogeneratedcardpinenabled;
    private String servicecode;
    private Long cardnumbergenalgorithmid;
    private Long cardpingenalgorithmid;
    private Boolean cardpinwithresponseenabled;
    private boolean allowbulkimportofcards;
    private long cardprogramgrouptypeid = 100L;
    private String customcardpinscriptname;
    private LocalDateTime creationdate;
    private Long createdby;
    private LocalDateTime lastmodifieddate;
    private Long lastmodifiedby;
    private String programdescription;
    private Integer cardpinlength;
    private Boolean uniquecardpin;
    private Short cardpinalphanumeric;
    private Long encryptionalgorithmid;
    private Boolean allowreactivateofexpriedcard;
    private Boolean allowcardnumberforcreateandissue;
    private Boolean autogeneratedbookletenabled;
    private String bookletbinoriincode;
    private String bookletprefix;
    private Integer cardnumberlength;
    private Boolean corporateoverrideenabled;
    private Long corporateid;
    private Boolean uniqueidgenerationenabled;
    private String activationurl;
    private Integer uidlength = 16;
    private Integer binoriincodelength = 6;
    private Boolean allowreissueacrosscpgs;
    private Boolean consideroutletcurrency;
    private String notes;
    private BigDecimal maxreloadableamount;
    private BigDecimal maxcardbalance;
    private Boolean allowmultiplecardclaimfordifferentcustomers;
    private Boolean enableinmarketplace;
    private Boolean isuidnumeric;
    private Boolean assignqccardtoextcard;
    private Boolean donotenforcesvcmapping;
    private Integer ubeformat;
    private Long cardprogramgroupsubtypeid;
    private Boolean enablesequencenumgen;
    private String sequencenumberprefix;
    private String claimcodeprefix;
    private Integer bookletsize;
    private Long pinvalidity;
    private QcEgmsCardIssuer qcEgmsCardIssuer;
    private QcEgmsCardprogramgroupStatusLk qcEgmsCardprogramgroupStatusLk;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`CARDPROGRAMGROUPID`", nullable = false, scale = 0, precision = 19)
    public Long getCardprogramgroupid() {
        return this.cardprogramgroupid;
    }

    public void setCardprogramgroupid(Long cardprogramgroupid) {
        this.cardprogramgroupid = cardprogramgroupid;
    }

    @Column(name = "`CARDPROGRAMDESC`", nullable = true, length = 128)
    public String getCardprogramdesc() {
        return this.cardprogramdesc;
    }

    public void setCardprogramdesc(String cardprogramdesc) {
        this.cardprogramdesc = cardprogramdesc;
    }

    @Column(name = "`ISSUERID`", nullable = false, scale = 0, precision = 19)
    public long getIssuerid() {
        return this.issuerid;
    }

    public void setIssuerid(long issuerid) {
        this.issuerid = issuerid;
    }

    @Column(name = "`LAUNCHDATE`", nullable = true)
    public LocalDateTime getLaunchdate() {
        return this.launchdate;
    }

    public void setLaunchdate(LocalDateTime launchdate) {
        this.launchdate = launchdate;
    }

    @Column(name = "`EXPIRYDATE`", nullable = true)
    public LocalDateTime getExpirydate() {
        return this.expirydate;
    }

    public void setExpirydate(LocalDateTime expirydate) {
        this.expirydate = expirydate;
    }

    @Column(name = "`CARDPROGRAMGROUPSTATUSID`", nullable = false, scale = 0, precision = 19)
    public long getCardprogramgroupstatusid() {
        return this.cardprogramgroupstatusid;
    }

    public void setCardprogramgroupstatusid(long cardprogramgroupstatusid) {
        this.cardprogramgroupstatusid = cardprogramgroupstatusid;
    }

    @Column(name = "`CARDTYPEID`", nullable = true, scale = 0, precision = 19)
    public Long getCardtypeid() {
        return this.cardtypeid;
    }

    public void setCardtypeid(Long cardtypeid) {
        this.cardtypeid = cardtypeid;
    }

    @Column(name = "`BINORIINCODE`", nullable = true, length = 6)
    public String getBinoriincode() {
        return this.binoriincode;
    }

    public void setBinoriincode(String binoriincode) {
        this.binoriincode = binoriincode;
    }

    @Column(name = "`MERCHANTPREFIX`", nullable = true, length = 3)
    public String getMerchantprefix() {
        return this.merchantprefix;
    }

    public void setMerchantprefix(String merchantprefix) {
        this.merchantprefix = merchantprefix;
    }

    @Column(name = "`AUTOGENERATEDCARDENABLED`", nullable = true)
    public Boolean getAutogeneratedcardenabled() {
        return this.autogeneratedcardenabled;
    }

    public void setAutogeneratedcardenabled(Boolean autogeneratedcardenabled) {
        this.autogeneratedcardenabled = autogeneratedcardenabled;
    }

    @Column(name = "`AUTOGENERATEDCARDPINENABLED`", nullable = true)
    public Boolean getAutogeneratedcardpinenabled() {
        return this.autogeneratedcardpinenabled;
    }

    public void setAutogeneratedcardpinenabled(Boolean autogeneratedcardpinenabled) {
        this.autogeneratedcardpinenabled = autogeneratedcardpinenabled;
    }

    @Column(name = "`SERVICECODE`", nullable = true, length = 3)
    public String getServicecode() {
        return this.servicecode;
    }

    public void setServicecode(String servicecode) {
        this.servicecode = servicecode;
    }

    @Column(name = "`CARDNUMBERGENALGORITHMID`", nullable = true, scale = 0, precision = 19)
    public Long getCardnumbergenalgorithmid() {
        return this.cardnumbergenalgorithmid;
    }

    public void setCardnumbergenalgorithmid(Long cardnumbergenalgorithmid) {
        this.cardnumbergenalgorithmid = cardnumbergenalgorithmid;
    }

    @Column(name = "`CARDPINGENALGORITHMID`", nullable = true, scale = 0, precision = 19)
    public Long getCardpingenalgorithmid() {
        return this.cardpingenalgorithmid;
    }

    public void setCardpingenalgorithmid(Long cardpingenalgorithmid) {
        this.cardpingenalgorithmid = cardpingenalgorithmid;
    }

    @Column(name = "`CARDPINWITHRESPONSEENABLED`", nullable = true)
    public Boolean getCardpinwithresponseenabled() {
        return this.cardpinwithresponseenabled;
    }

    public void setCardpinwithresponseenabled(Boolean cardpinwithresponseenabled) {
        this.cardpinwithresponseenabled = cardpinwithresponseenabled;
    }

    @Column(name = "`ALLOWBULKIMPORTOFCARDS`", nullable = false)
    public boolean isAllowbulkimportofcards() {
        return this.allowbulkimportofcards;
    }

    public void setAllowbulkimportofcards(boolean allowbulkimportofcards) {
        this.allowbulkimportofcards = allowbulkimportofcards;
    }

    @Column(name = "`CARDPROGRAMGROUPTYPEID`", nullable = false, scale = 0, precision = 19)
    public long getCardprogramgrouptypeid() {
        return this.cardprogramgrouptypeid;
    }

    public void setCardprogramgrouptypeid(long cardprogramgrouptypeid) {
        this.cardprogramgrouptypeid = cardprogramgrouptypeid;
    }

    @Column(name = "`CUSTOMCARDPINSCRIPTNAME`", nullable = true, length = 128)
    public String getCustomcardpinscriptname() {
        return this.customcardpinscriptname;
    }

    public void setCustomcardpinscriptname(String customcardpinscriptname) {
        this.customcardpinscriptname = customcardpinscriptname;
    }

    @Column(name = "`CREATIONDATE`", nullable = true)
    public LocalDateTime getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(LocalDateTime creationdate) {
        this.creationdate = creationdate;
    }

    @Column(name = "`CREATEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(Long createdby) {
        this.createdby = createdby;
    }

    @Column(name = "`LASTMODIFIEDDATE`", nullable = true)
    public LocalDateTime getLastmodifieddate() {
        return this.lastmodifieddate;
    }

    public void setLastmodifieddate(LocalDateTime lastmodifieddate) {
        this.lastmodifieddate = lastmodifieddate;
    }

    @Column(name = "`LASTMODIFIEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getLastmodifiedby() {
        return this.lastmodifiedby;
    }

    public void setLastmodifiedby(Long lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    @Column(name = "`PROGRAMDESCRIPTION`", nullable = true, length = 500)
    public String getProgramdescription() {
        return this.programdescription;
    }

    public void setProgramdescription(String programdescription) {
        this.programdescription = programdescription;
    }

    @Column(name = "`CARDPINLENGTH`", nullable = true, scale = 0, precision = 10)
    public Integer getCardpinlength() {
        return this.cardpinlength;
    }

    public void setCardpinlength(Integer cardpinlength) {
        this.cardpinlength = cardpinlength;
    }

    @Column(name = "`UNIQUECARDPIN`", nullable = true)
    public Boolean getUniquecardpin() {
        return this.uniquecardpin;
    }

    public void setUniquecardpin(Boolean uniquecardpin) {
        this.uniquecardpin = uniquecardpin;
    }

    @Column(name = "`CARDPINALPHANUMERIC`", nullable = true, scale = 0, precision = 5)
    public Short getCardpinalphanumeric() {
        return this.cardpinalphanumeric;
    }

    public void setCardpinalphanumeric(Short cardpinalphanumeric) {
        this.cardpinalphanumeric = cardpinalphanumeric;
    }

    @Column(name = "`ENCRYPTIONALGORITHMID`", nullable = true, scale = 0, precision = 19)
    public Long getEncryptionalgorithmid() {
        return this.encryptionalgorithmid;
    }

    public void setEncryptionalgorithmid(Long encryptionalgorithmid) {
        this.encryptionalgorithmid = encryptionalgorithmid;
    }

    @Column(name = "`ALLOWREACTIVATEOFEXPRIEDCARD`", nullable = true)
    public Boolean getAllowreactivateofexpriedcard() {
        return this.allowreactivateofexpriedcard;
    }

    public void setAllowreactivateofexpriedcard(Boolean allowreactivateofexpriedcard) {
        this.allowreactivateofexpriedcard = allowreactivateofexpriedcard;
    }

    @Column(name = "`ALLOWCARDNUMBERFORCREATEANDISSUE`", nullable = true)
    public Boolean getAllowcardnumberforcreateandissue() {
        return this.allowcardnumberforcreateandissue;
    }

    public void setAllowcardnumberforcreateandissue(Boolean allowcardnumberforcreateandissue) {
        this.allowcardnumberforcreateandissue = allowcardnumberforcreateandissue;
    }

    @Column(name = "`AUTOGENERATEDBOOKLETENABLED`", nullable = true)
    public Boolean getAutogeneratedbookletenabled() {
        return this.autogeneratedbookletenabled;
    }

    public void setAutogeneratedbookletenabled(Boolean autogeneratedbookletenabled) {
        this.autogeneratedbookletenabled = autogeneratedbookletenabled;
    }

    @Column(name = "`BOOKLETBINORIINCODE`", nullable = true, length = 6)
    public String getBookletbinoriincode() {
        return this.bookletbinoriincode;
    }

    public void setBookletbinoriincode(String bookletbinoriincode) {
        this.bookletbinoriincode = bookletbinoriincode;
    }

    @Column(name = "`BOOKLETPREFIX`", nullable = true, length = 3)
    public String getBookletprefix() {
        return this.bookletprefix;
    }

    public void setBookletprefix(String bookletprefix) {
        this.bookletprefix = bookletprefix;
    }

    @Column(name = "`CARDNUMBERLENGTH`", nullable = true, scale = 0, precision = 10)
    public Integer getCardnumberlength() {
        return this.cardnumberlength;
    }

    public void setCardnumberlength(Integer cardnumberlength) {
        this.cardnumberlength = cardnumberlength;
    }

    @Column(name = "`CORPORATEOVERRIDEENABLED`", nullable = true)
    public Boolean getCorporateoverrideenabled() {
        return this.corporateoverrideenabled;
    }

    public void setCorporateoverrideenabled(Boolean corporateoverrideenabled) {
        this.corporateoverrideenabled = corporateoverrideenabled;
    }

    @Column(name = "`CORPORATEID`", nullable = true, scale = 0, precision = 19)
    public Long getCorporateid() {
        return this.corporateid;
    }

    public void setCorporateid(Long corporateid) {
        this.corporateid = corporateid;
    }

    @Column(name = "`UNIQUEIDGENERATIONENABLED`", nullable = true)
    public Boolean getUniqueidgenerationenabled() {
        return this.uniqueidgenerationenabled;
    }

    public void setUniqueidgenerationenabled(Boolean uniqueidgenerationenabled) {
        this.uniqueidgenerationenabled = uniqueidgenerationenabled;
    }

    @Column(name = "`ACTIVATIONURL`", nullable = true, length = 500)
    public String getActivationurl() {
        return this.activationurl;
    }

    public void setActivationurl(String activationurl) {
        this.activationurl = activationurl;
    }

    @Column(name = "`UIDLENGTH`", nullable = true, scale = 0, precision = 10)
    public Integer getUidlength() {
        return this.uidlength;
    }

    public void setUidlength(Integer uidlength) {
        this.uidlength = uidlength;
    }

    @Column(name = "`BINORIINCODELENGTH`", nullable = true, scale = 0, precision = 10)
    public Integer getBinoriincodelength() {
        return this.binoriincodelength;
    }

    public void setBinoriincodelength(Integer binoriincodelength) {
        this.binoriincodelength = binoriincodelength;
    }

    @Column(name = "`ALLOWREISSUEACROSSCPGS`", nullable = true)
    public Boolean getAllowreissueacrosscpgs() {
        return this.allowreissueacrosscpgs;
    }

    public void setAllowreissueacrosscpgs(Boolean allowreissueacrosscpgs) {
        this.allowreissueacrosscpgs = allowreissueacrosscpgs;
    }

    @Column(name = "`CONSIDEROUTLETCURRENCY`", nullable = true)
    public Boolean getConsideroutletcurrency() {
        return this.consideroutletcurrency;
    }

    public void setConsideroutletcurrency(Boolean consideroutletcurrency) {
        this.consideroutletcurrency = consideroutletcurrency;
    }

    @Column(name = "`NOTES`", nullable = true, length = 500)
    public String getNotes() {
        return this.notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Column(name = "`MAXRELOADABLEAMOUNT`", nullable = true, scale = 4, precision = 22)
    public BigDecimal getMaxreloadableamount() {
        return this.maxreloadableamount;
    }

    public void setMaxreloadableamount(BigDecimal maxreloadableamount) {
        this.maxreloadableamount = maxreloadableamount;
    }

    @Column(name = "`MAXCARDBALANCE`", nullable = true, scale = 4, precision = 22)
    public BigDecimal getMaxcardbalance() {
        return this.maxcardbalance;
    }

    public void setMaxcardbalance(BigDecimal maxcardbalance) {
        this.maxcardbalance = maxcardbalance;
    }

    @Column(name = "`ALLOWMULTIPLECARDCLAIMFORDIFFERENTCUSTOMERS`", nullable = true)
    public Boolean getAllowmultiplecardclaimfordifferentcustomers() {
        return this.allowmultiplecardclaimfordifferentcustomers;
    }

    public void setAllowmultiplecardclaimfordifferentcustomers(Boolean allowmultiplecardclaimfordifferentcustomers) {
        this.allowmultiplecardclaimfordifferentcustomers = allowmultiplecardclaimfordifferentcustomers;
    }

    @Column(name = "`ENABLEINMARKETPLACE`", nullable = true)
    public Boolean getEnableinmarketplace() {
        return this.enableinmarketplace;
    }

    public void setEnableinmarketplace(Boolean enableinmarketplace) {
        this.enableinmarketplace = enableinmarketplace;
    }

    @Column(name = "`ISUIDNUMERIC`", nullable = true)
    public Boolean getIsuidnumeric() {
        return this.isuidnumeric;
    }

    public void setIsuidnumeric(Boolean isuidnumeric) {
        this.isuidnumeric = isuidnumeric;
    }

    @Column(name = "`ASSIGNQCCARDTOEXTCARD`", nullable = true)
    public Boolean getAssignqccardtoextcard() {
        return this.assignqccardtoextcard;
    }

    public void setAssignqccardtoextcard(Boolean assignqccardtoextcard) {
        this.assignqccardtoextcard = assignqccardtoextcard;
    }

    @Column(name = "`DONOTENFORCESVCMAPPING`", nullable = true)
    public Boolean getDonotenforcesvcmapping() {
        return this.donotenforcesvcmapping;
    }

    public void setDonotenforcesvcmapping(Boolean donotenforcesvcmapping) {
        this.donotenforcesvcmapping = donotenforcesvcmapping;
    }

    @Column(name = "`UBEFORMAT`", nullable = true, scale = 0, precision = 10)
    public Integer getUbeformat() {
        return this.ubeformat;
    }

    public void setUbeformat(Integer ubeformat) {
        this.ubeformat = ubeformat;
    }

    @Column(name = "`CARDPROGRAMGROUPSUBTYPEID`", nullable = true, scale = 0, precision = 19)
    public Long getCardprogramgroupsubtypeid() {
        return this.cardprogramgroupsubtypeid;
    }

    public void setCardprogramgroupsubtypeid(Long cardprogramgroupsubtypeid) {
        this.cardprogramgroupsubtypeid = cardprogramgroupsubtypeid;
    }

    @Column(name = "`ENABLESEQUENCENUMGEN`", nullable = true)
    public Boolean getEnablesequencenumgen() {
        return this.enablesequencenumgen;
    }

    public void setEnablesequencenumgen(Boolean enablesequencenumgen) {
        this.enablesequencenumgen = enablesequencenumgen;
    }

    @Column(name = "`SEQUENCENUMBERPREFIX`", nullable = true, length = 15)
    public String getSequencenumberprefix() {
        return this.sequencenumberprefix;
    }

    public void setSequencenumberprefix(String sequencenumberprefix) {
        this.sequencenumberprefix = sequencenumberprefix;
    }

    @Column(name = "`CLAIMCODEPREFIX`", nullable = true, length = 15)
    public String getClaimcodeprefix() {
        return this.claimcodeprefix;
    }

    public void setClaimcodeprefix(String claimcodeprefix) {
        this.claimcodeprefix = claimcodeprefix;
    }

    @Column(name = "`BOOKLETSIZE`", nullable = true, scale = 0, precision = 10)
    public Integer getBookletsize() {
        return this.bookletsize;
    }

    public void setBookletsize(Integer bookletsize) {
        this.bookletsize = bookletsize;
    }

    @Column(name = "`PINVALIDITY`", nullable = true, scale = 0, precision = 19)
    public Long getPinvalidity() {
        return this.pinvalidity;
    }

    public void setPinvalidity(Long pinvalidity) {
        this.pinvalidity = pinvalidity;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`ISSUERID`", referencedColumnName = "`ISSUERID`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_Card_Programs_Card_Issuer`"))
    @Fetch(FetchMode.JOIN)
    public QcEgmsCardIssuer getQcEgmsCardIssuer() {
        return this.qcEgmsCardIssuer;
    }

    public void setQcEgmsCardIssuer(QcEgmsCardIssuer qcEgmsCardIssuer) {
        if(qcEgmsCardIssuer != null) {
            this.issuerid = qcEgmsCardIssuer.getIssuerid();
        }

        this.qcEgmsCardIssuer = qcEgmsCardIssuer;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`CARDPROGRAMGROUPSTATUSID`", referencedColumnName = "`CARDPROGRAMGROUPSTATUSID`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_QC_EGMS_CARD_PROGRAM_GROUPS_QC_EGMS_CARDPROGRAMGROUP_STATUS_LK`"))
    @Fetch(FetchMode.JOIN)
    public QcEgmsCardprogramgroupStatusLk getQcEgmsCardprogramgroupStatusLk() {
        return this.qcEgmsCardprogramgroupStatusLk;
    }

    public void setQcEgmsCardprogramgroupStatusLk(QcEgmsCardprogramgroupStatusLk qcEgmsCardprogramgroupStatusLk) {
        if(qcEgmsCardprogramgroupStatusLk != null) {
            this.cardprogramgroupstatusid = qcEgmsCardprogramgroupStatusLk.getCardprogramgroupstatusid();
        }

        this.qcEgmsCardprogramgroupStatusLk = qcEgmsCardprogramgroupStatusLk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QcEgmsCardProgramGroups)) return false;
        final QcEgmsCardProgramGroups qcEgmsCardProgramGroups = (QcEgmsCardProgramGroups) o;
        return Objects.equals(getCardprogramgroupid(), qcEgmsCardProgramGroups.getCardprogramgroupid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCardprogramgroupid());
    }
}