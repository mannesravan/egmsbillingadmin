/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * QcEgmsCardprogramgroupStatusLk generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`QC_EGMS_CARDPROGRAMGROUP_STATUS_LK`")
public class QcEgmsCardprogramgroupStatusLk implements Serializable {

    private Long cardprogramgroupstatusid;
    private String cardprogramgroupstatus;

    @Id
    @Column(name = "`CARDPROGRAMGROUPSTATUSID`", nullable = false, scale = 0, precision = 19)
    public Long getCardprogramgroupstatusid() {
        return this.cardprogramgroupstatusid;
    }

    public void setCardprogramgroupstatusid(Long cardprogramgroupstatusid) {
        this.cardprogramgroupstatusid = cardprogramgroupstatusid;
    }

    @Column(name = "`CARDPROGRAMGROUPSTATUS`", nullable = false, length = 128)
    public String getCardprogramgroupstatus() {
        return this.cardprogramgroupstatus;
    }

    public void setCardprogramgroupstatus(String cardprogramgroupstatus) {
        this.cardprogramgroupstatus = cardprogramgroupstatus;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QcEgmsCardprogramgroupStatusLk)) return false;
        final QcEgmsCardprogramgroupStatusLk qcEgmsCardprogramgroupStatusLk = (QcEgmsCardprogramgroupStatusLk) o;
        return Objects.equals(getCardprogramgroupstatusid(), qcEgmsCardprogramgroupStatusLk.getCardprogramgroupstatusid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCardprogramgroupstatusid());
    }
}