/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * QcEgmsBillingSlabComputationTypeLk generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`QC_EGMS_BILLING_SLAB_COMPUTATION_TYPE_LK`", uniqueConstraints = {
            @UniqueConstraint(name = "`UQ__QC_EGMS___B517FDCD1997263A`", columnNames = {"`SLABCOMPUTATIONTYPENAME`"})})
public class QcEgmsBillingSlabComputationTypeLk implements Serializable {

    private Long slabcomputationtypeid;
    private String slabcomputationtypename;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`SLABCOMPUTATIONTYPEID`", nullable = false, scale = 0, precision = 19)
    public Long getSlabcomputationtypeid() {
        return this.slabcomputationtypeid;
    }

    public void setSlabcomputationtypeid(Long slabcomputationtypeid) {
        this.slabcomputationtypeid = slabcomputationtypeid;
    }

    @Column(name = "`SLABCOMPUTATIONTYPENAME`", nullable = false, length = 32)
    public String getSlabcomputationtypename() {
        return this.slabcomputationtypename;
    }

    public void setSlabcomputationtypename(String slabcomputationtypename) {
        this.slabcomputationtypename = slabcomputationtypename;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QcEgmsBillingSlabComputationTypeLk)) return false;
        final QcEgmsBillingSlabComputationTypeLk qcEgmsBillingSlabComputationTypeLk = (QcEgmsBillingSlabComputationTypeLk) o;
        return Objects.equals(getSlabcomputationtypeid(), qcEgmsBillingSlabComputationTypeLk.getSlabcomputationtypeid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSlabcomputationtypeid());
    }
}