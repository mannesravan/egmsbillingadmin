/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * QcEgmsBillingFinancialYearConfig generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`QC_EGMS_BILLING_FINANCIAL_YEAR_CONFIG`")
public class QcEgmsBillingFinancialYearConfig implements Serializable {

    private Long financialyearid;
    private String financialyearname;
    private Long financialyearfirsthalf;
    private LocalDateTime financialyearstartdate;
    private LocalDateTime financialyearenddate;
    private BigDecimal servicetax;
    private BigDecimal educationcess;
    private BigDecimal secondaryeducationcess;
    private BigDecimal vat;
    private LocalDateTime creationdate;
    private Long createdby;
    private LocalDateTime lastmodifieddate;
    private Long lastmodifiedby;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`FINANCIALYEARID`", nullable = false, scale = 0, precision = 19)
    public Long getFinancialyearid() {
        return this.financialyearid;
    }

    public void setFinancialyearid(Long financialyearid) {
        this.financialyearid = financialyearid;
    }

    @Column(name = "`FINANCIALYEARNAME`", nullable = true, length = 32)
    public String getFinancialyearname() {
        return this.financialyearname;
    }

    public void setFinancialyearname(String financialyearname) {
        this.financialyearname = financialyearname;
    }

    @Column(name = "`FINANCIALYEARFIRSTHALF`", nullable = true, scale = 0, precision = 19)
    public Long getFinancialyearfirsthalf() {
        return this.financialyearfirsthalf;
    }

    public void setFinancialyearfirsthalf(Long financialyearfirsthalf) {
        this.financialyearfirsthalf = financialyearfirsthalf;
    }

    @Column(name = "`FINANCIALYEARSTARTDATE`", nullable = true)
    public LocalDateTime getFinancialyearstartdate() {
        return this.financialyearstartdate;
    }

    public void setFinancialyearstartdate(LocalDateTime financialyearstartdate) {
        this.financialyearstartdate = financialyearstartdate;
    }

    @Column(name = "`FINANCIALYEARENDDATE`", nullable = true)
    public LocalDateTime getFinancialyearenddate() {
        return this.financialyearenddate;
    }

    public void setFinancialyearenddate(LocalDateTime financialyearenddate) {
        this.financialyearenddate = financialyearenddate;
    }

    @Column(name = "`SERVICETAX`", nullable = true, scale = 4, precision = 22)
    public BigDecimal getServicetax() {
        return this.servicetax;
    }

    public void setServicetax(BigDecimal servicetax) {
        this.servicetax = servicetax;
    }

    @Column(name = "`EDUCATIONCESS`", nullable = true, scale = 4, precision = 22)
    public BigDecimal getEducationcess() {
        return this.educationcess;
    }

    public void setEducationcess(BigDecimal educationcess) {
        this.educationcess = educationcess;
    }

    @Column(name = "`SECONDARYEDUCATIONCESS`", nullable = true, scale = 4, precision = 22)
    public BigDecimal getSecondaryeducationcess() {
        return this.secondaryeducationcess;
    }

    public void setSecondaryeducationcess(BigDecimal secondaryeducationcess) {
        this.secondaryeducationcess = secondaryeducationcess;
    }

    @Column(name = "`VAT`", nullable = true, scale = 4, precision = 22)
    public BigDecimal getVat() {
        return this.vat;
    }

    public void setVat(BigDecimal vat) {
        this.vat = vat;
    }

    @Column(name = "`CREATIONDATE`", nullable = true)
    public LocalDateTime getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(LocalDateTime creationdate) {
        this.creationdate = creationdate;
    }

    @Column(name = "`CREATEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(Long createdby) {
        this.createdby = createdby;
    }

    @Column(name = "`LASTMODIFIEDDATE`", nullable = true)
    public LocalDateTime getLastmodifieddate() {
        return this.lastmodifieddate;
    }

    public void setLastmodifieddate(LocalDateTime lastmodifieddate) {
        this.lastmodifieddate = lastmodifieddate;
    }

    @Column(name = "`LASTMODIFIEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getLastmodifiedby() {
        return this.lastmodifiedby;
    }

    public void setLastmodifiedby(Long lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QcEgmsBillingFinancialYearConfig)) return false;
        final QcEgmsBillingFinancialYearConfig qcEgmsBillingFinancialYearConfig = (QcEgmsBillingFinancialYearConfig) o;
        return Objects.equals(getFinancialyearid(), qcEgmsBillingFinancialYearConfig.getFinancialyearid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFinancialyearid());
    }
}