/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * QcEgmsBillingGroupMerchantOverride generated by WaveMaker Studio.
 */
@Entity
@Table(name = "`QC_EGMS_BILLING_GROUP_MERCHANT_OVERRIDE`")
public class QcEgmsBillingGroupMerchantOverride implements Serializable {

    private Long billinggroupmerchantoverrideid;
    private long billinggrouppkid;
    private long billinggroupid;
    private long merchantid;
    private long paramid;
    private String paramvalue;
    private LocalDateTime creationdate;
    private Long createdby;
    private LocalDateTime lastmodifieddate;
    private Long lastmodifiedby;
    private QcEgmsBillingGroups qcEgmsBillingGroups;
    private QcEgmsBillingParamMaster qcEgmsBillingParamMaster;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "`BILLINGGROUPMERCHANTOVERRIDEID`", nullable = false, scale = 0, precision = 19)
    public Long getBillinggroupmerchantoverrideid() {
        return this.billinggroupmerchantoverrideid;
    }

    public void setBillinggroupmerchantoverrideid(Long billinggroupmerchantoverrideid) {
        this.billinggroupmerchantoverrideid = billinggroupmerchantoverrideid;
    }

    @Column(name = "`BILLINGGROUPPKID`", nullable = false, scale = 0, precision = 19)
    public long getBillinggrouppkid() {
        return this.billinggrouppkid;
    }

    public void setBillinggrouppkid(long billinggrouppkid) {
        this.billinggrouppkid = billinggrouppkid;
    }

    @Column(name = "`BILLINGGROUPID`", nullable = false, scale = 0, precision = 19)
    public long getBillinggroupid() {
        return this.billinggroupid;
    }

    public void setBillinggroupid(long billinggroupid) {
        this.billinggroupid = billinggroupid;
    }

    @Column(name = "`MERCHANTID`", nullable = false, scale = 0, precision = 19)
    public long getMerchantid() {
        return this.merchantid;
    }

    public void setMerchantid(long merchantid) {
        this.merchantid = merchantid;
    }

    @Column(name = "`PARAMID`", nullable = false, scale = 0, precision = 19)
    public long getParamid() {
        return this.paramid;
    }

    public void setParamid(long paramid) {
        this.paramid = paramid;
    }

    @Column(name = "`PARAMVALUE`", nullable = true, length = 255)
    public String getParamvalue() {
        return this.paramvalue;
    }

    public void setParamvalue(String paramvalue) {
        this.paramvalue = paramvalue;
    }

    @Column(name = "`CREATIONDATE`", nullable = true)
    public LocalDateTime getCreationdate() {
        return this.creationdate;
    }

    public void setCreationdate(LocalDateTime creationdate) {
        this.creationdate = creationdate;
    }

    @Column(name = "`CREATEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getCreatedby() {
        return this.createdby;
    }

    public void setCreatedby(Long createdby) {
        this.createdby = createdby;
    }

    @Column(name = "`LASTMODIFIEDDATE`", nullable = true)
    public LocalDateTime getLastmodifieddate() {
        return this.lastmodifieddate;
    }

    public void setLastmodifieddate(LocalDateTime lastmodifieddate) {
        this.lastmodifieddate = lastmodifieddate;
    }

    @Column(name = "`LASTMODIFIEDBY`", nullable = true, scale = 0, precision = 19)
    public Long getLastmodifiedby() {
        return this.lastmodifiedby;
    }

    public void setLastmodifiedby(Long lastmodifiedby) {
        this.lastmodifiedby = lastmodifiedby;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`BILLINGGROUPPKID`", referencedColumnName = "`BILLINGGROUPPKID`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_QC_EGMS_BILLING_GROUP_MERCHANT_OVERRIDE_BILLING_GROUP`"))
    @Fetch(FetchMode.JOIN)
    public QcEgmsBillingGroups getQcEgmsBillingGroups() {
        return this.qcEgmsBillingGroups;
    }

    public void setQcEgmsBillingGroups(QcEgmsBillingGroups qcEgmsBillingGroups) {
        if(qcEgmsBillingGroups != null) {
            this.billinggrouppkid = qcEgmsBillingGroups.getBillinggrouppkid();
        }

        this.qcEgmsBillingGroups = qcEgmsBillingGroups;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "`PARAMID`", referencedColumnName = "`PARAMID`", insertable = false, updatable = false, foreignKey = @ForeignKey(name = "`FK_QC_EGMS_BILLING_GROUP_MERCHANT_OVERRIDE_PARAM_MASTER`"))
    @Fetch(FetchMode.JOIN)
    public QcEgmsBillingParamMaster getQcEgmsBillingParamMaster() {
        return this.qcEgmsBillingParamMaster;
    }

    public void setQcEgmsBillingParamMaster(QcEgmsBillingParamMaster qcEgmsBillingParamMaster) {
        if(qcEgmsBillingParamMaster != null) {
            this.paramid = qcEgmsBillingParamMaster.getParamid();
        }

        this.qcEgmsBillingParamMaster = qcEgmsBillingParamMaster;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QcEgmsBillingGroupMerchantOverride)) return false;
        final QcEgmsBillingGroupMerchantOverride qcEgmsBillingGroupMerchantOverride = (QcEgmsBillingGroupMerchantOverride) o;
        return Objects.equals(getBillinggroupmerchantoverrideid(), qcEgmsBillingGroupMerchantOverride.getBillinggroupmerchantoverrideid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBillinggroupmerchantoverrideid());
    }
}