/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.wavemaker.commons.InvalidInputException;
import com.wavemaker.commons.MessageResource;
import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingMonthlyManualInput;


/**
 * ServiceImpl object for domain model class QcEgmsBillingMonthlyManualInput.
 *
 * @see QcEgmsBillingMonthlyManualInput
 */
@Service("WM_Billing_Admin.QcEgmsBillingMonthlyManualInputService")
@Validated
public class QcEgmsBillingMonthlyManualInputServiceImpl implements QcEgmsBillingMonthlyManualInputService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QcEgmsBillingMonthlyManualInputServiceImpl.class);


    @Autowired
    @Qualifier("WM_Billing_Admin.QcEgmsBillingMonthlyManualInputDao")
    private WMGenericDao<QcEgmsBillingMonthlyManualInput, Long> wmGenericDao;

    @Autowired
    @Qualifier("wmAppObjectMapper")
    private ObjectMapper objectMapper;


    public void setWMGenericDao(WMGenericDao<QcEgmsBillingMonthlyManualInput, Long> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingMonthlyManualInput create(QcEgmsBillingMonthlyManualInput qcEgmsBillingMonthlyManualInput) {
        LOGGER.debug("Creating a new QcEgmsBillingMonthlyManualInput with information: {}", qcEgmsBillingMonthlyManualInput);

        QcEgmsBillingMonthlyManualInput qcEgmsBillingMonthlyManualInputCreated = this.wmGenericDao.create(qcEgmsBillingMonthlyManualInput);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(qcEgmsBillingMonthlyManualInputCreated);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingMonthlyManualInput getById(Long qcegmsbillingmonthlymanualinputId) {
        LOGGER.debug("Finding QcEgmsBillingMonthlyManualInput by id: {}", qcegmsbillingmonthlymanualinputId);
        return this.wmGenericDao.findById(qcegmsbillingmonthlymanualinputId);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingMonthlyManualInput findById(Long qcegmsbillingmonthlymanualinputId) {
        LOGGER.debug("Finding QcEgmsBillingMonthlyManualInput by id: {}", qcegmsbillingmonthlymanualinputId);
        try {
            return this.wmGenericDao.findById(qcegmsbillingmonthlymanualinputId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No QcEgmsBillingMonthlyManualInput found with id: {}", qcegmsbillingmonthlymanualinputId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public List<QcEgmsBillingMonthlyManualInput> findByMultipleIds(List<Long> qcegmsbillingmonthlymanualinputIds, boolean orderedReturn) {
        LOGGER.debug("Finding QcEgmsBillingMonthlyManualInputs by ids: {}", qcegmsbillingmonthlymanualinputIds);

        return this.wmGenericDao.findByMultipleIds(qcegmsbillingmonthlymanualinputIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingMonthlyManualInput update(QcEgmsBillingMonthlyManualInput qcEgmsBillingMonthlyManualInput) {
        LOGGER.debug("Updating QcEgmsBillingMonthlyManualInput with information: {}", qcEgmsBillingMonthlyManualInput);

        this.wmGenericDao.update(qcEgmsBillingMonthlyManualInput);
        this.wmGenericDao.refresh(qcEgmsBillingMonthlyManualInput);

        return qcEgmsBillingMonthlyManualInput;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingMonthlyManualInput partialUpdate(Long qcegmsbillingmonthlymanualinputId, Map<String, Object>qcEgmsBillingMonthlyManualInputPatch) {
        LOGGER.debug("Partially Updating the QcEgmsBillingMonthlyManualInput with id: {}", qcegmsbillingmonthlymanualinputId);

        QcEgmsBillingMonthlyManualInput qcEgmsBillingMonthlyManualInput = getById(qcegmsbillingmonthlymanualinputId);

        try {
            ObjectReader qcEgmsBillingMonthlyManualInputReader = this.objectMapper.reader().forType(QcEgmsBillingMonthlyManualInput.class).withValueToUpdate(qcEgmsBillingMonthlyManualInput);
            qcEgmsBillingMonthlyManualInput = qcEgmsBillingMonthlyManualInputReader.readValue(this.objectMapper.writeValueAsString(qcEgmsBillingMonthlyManualInputPatch));
        } catch (IOException ex) {
            LOGGER.debug("There was a problem in applying the patch: {}", qcEgmsBillingMonthlyManualInputPatch, ex);
            throw new InvalidInputException("Could not apply patch",ex);
        }

        qcEgmsBillingMonthlyManualInput = update(qcEgmsBillingMonthlyManualInput);

        return qcEgmsBillingMonthlyManualInput;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingMonthlyManualInput delete(Long qcegmsbillingmonthlymanualinputId) {
        LOGGER.debug("Deleting QcEgmsBillingMonthlyManualInput with id: {}", qcegmsbillingmonthlymanualinputId);
        QcEgmsBillingMonthlyManualInput deleted = this.wmGenericDao.findById(qcegmsbillingmonthlymanualinputId);
        if (deleted == null) {
            LOGGER.debug("No QcEgmsBillingMonthlyManualInput found with id: {}", qcegmsbillingmonthlymanualinputId);
            throw new EntityNotFoundException(MessageResource.create("com.wavemaker.runtime.entity.not.found"), QcEgmsBillingMonthlyManualInput.class.getSimpleName(), qcegmsbillingmonthlymanualinputId);
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public void delete(QcEgmsBillingMonthlyManualInput qcEgmsBillingMonthlyManualInput) {
        LOGGER.debug("Deleting QcEgmsBillingMonthlyManualInput with {}", qcEgmsBillingMonthlyManualInput);
        this.wmGenericDao.delete(qcEgmsBillingMonthlyManualInput);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingMonthlyManualInput> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingMonthlyManualInputs");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingMonthlyManualInput> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingMonthlyManualInputs");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingMonthlyManualInput to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingMonthlyManualInput to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}