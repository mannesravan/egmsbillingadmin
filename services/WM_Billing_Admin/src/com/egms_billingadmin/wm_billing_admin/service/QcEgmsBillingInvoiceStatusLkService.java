/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingInvoiceStatusLk;
import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingInvoices;

/**
 * Service object for domain model class {@link QcEgmsBillingInvoiceStatusLk}.
 */
public interface QcEgmsBillingInvoiceStatusLkService {

    /**
     * Creates a new QcEgmsBillingInvoiceStatusLk. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsBillingInvoiceStatusLk if any.
     *
     * @param qcEgmsBillingInvoiceStatusLk Details of the QcEgmsBillingInvoiceStatusLk to be created; value cannot be null.
     * @return The newly created QcEgmsBillingInvoiceStatusLk.
     */
    QcEgmsBillingInvoiceStatusLk create(@Valid QcEgmsBillingInvoiceStatusLk qcEgmsBillingInvoiceStatusLk);


	/**
     * Returns QcEgmsBillingInvoiceStatusLk by given id if exists.
     *
     * @param qcegmsbillinginvoicestatuslkId The id of the QcEgmsBillingInvoiceStatusLk to get; value cannot be null.
     * @return QcEgmsBillingInvoiceStatusLk associated with the given qcegmsbillinginvoicestatuslkId.
	 * @throws EntityNotFoundException If no QcEgmsBillingInvoiceStatusLk is found.
     */
    QcEgmsBillingInvoiceStatusLk getById(Long qcegmsbillinginvoicestatuslkId);

    /**
     * Find and return the QcEgmsBillingInvoiceStatusLk by given id if exists, returns null otherwise.
     *
     * @param qcegmsbillinginvoicestatuslkId The id of the QcEgmsBillingInvoiceStatusLk to get; value cannot be null.
     * @return QcEgmsBillingInvoiceStatusLk associated with the given qcegmsbillinginvoicestatuslkId.
     */
    QcEgmsBillingInvoiceStatusLk findById(Long qcegmsbillinginvoicestatuslkId);

	/**
     * Find and return the list of QcEgmsBillingInvoiceStatusLks by given id's.
     *
     * If orderedReturn true, the return List is ordered and positional relative to the incoming ids.
     *
     * In case of unknown entities:
     *
     * If enabled, A null is inserted into the List at the proper position(s).
     * If disabled, the nulls are not put into the return List.
     *
     * @param qcegmsbillinginvoicestatuslkIds The id's of the QcEgmsBillingInvoiceStatusLk to get; value cannot be null.
     * @param orderedReturn Should the return List be ordered and positional in relation to the incoming ids?
     * @return QcEgmsBillingInvoiceStatusLks associated with the given qcegmsbillinginvoicestatuslkIds.
     */
    List<QcEgmsBillingInvoiceStatusLk> findByMultipleIds(List<Long> qcegmsbillinginvoicestatuslkIds, boolean orderedReturn);


    /**
     * Updates the details of an existing QcEgmsBillingInvoiceStatusLk. It replaces all fields of the existing QcEgmsBillingInvoiceStatusLk with the given qcEgmsBillingInvoiceStatusLk.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsBillingInvoiceStatusLk if any.
     *
     * @param qcEgmsBillingInvoiceStatusLk The details of the QcEgmsBillingInvoiceStatusLk to be updated; value cannot be null.
     * @return The updated QcEgmsBillingInvoiceStatusLk.
     * @throws EntityNotFoundException if no QcEgmsBillingInvoiceStatusLk is found with given input.
     */
    QcEgmsBillingInvoiceStatusLk update(@Valid QcEgmsBillingInvoiceStatusLk qcEgmsBillingInvoiceStatusLk);


    /**
     * Partially updates the details of an existing QcEgmsBillingInvoiceStatusLk. It updates only the
     * fields of the existing QcEgmsBillingInvoiceStatusLk which are passed in the qcEgmsBillingInvoiceStatusLkPatch.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsBillingInvoiceStatusLk if any.
     *
     * @param qcegmsbillinginvoicestatuslkId The id of the QcEgmsBillingInvoiceStatusLk to be deleted; value cannot be null.
     * @param qcEgmsBillingInvoiceStatusLkPatch The partial data of QcEgmsBillingInvoiceStatusLk which is supposed to be updated; value cannot be null.
     * @return The updated QcEgmsBillingInvoiceStatusLk.
     * @throws EntityNotFoundException if no QcEgmsBillingInvoiceStatusLk is found with given input.
     */
    QcEgmsBillingInvoiceStatusLk partialUpdate(Long qcegmsbillinginvoicestatuslkId, Map<String, Object> qcEgmsBillingInvoiceStatusLkPatch);

    /**
     * Deletes an existing QcEgmsBillingInvoiceStatusLk with the given id.
     *
     * @param qcegmsbillinginvoicestatuslkId The id of the QcEgmsBillingInvoiceStatusLk to be deleted; value cannot be null.
     * @return The deleted QcEgmsBillingInvoiceStatusLk.
     * @throws EntityNotFoundException if no QcEgmsBillingInvoiceStatusLk found with the given id.
     */
    QcEgmsBillingInvoiceStatusLk delete(Long qcegmsbillinginvoicestatuslkId);

    /**
     * Deletes an existing QcEgmsBillingInvoiceStatusLk with the given object.
     *
     * @param qcEgmsBillingInvoiceStatusLk The instance of the QcEgmsBillingInvoiceStatusLk to be deleted; value cannot be null.
     */
    void delete(QcEgmsBillingInvoiceStatusLk qcEgmsBillingInvoiceStatusLk);

    /**
     * Find all QcEgmsBillingInvoiceStatusLks matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
     *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
     *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching QcEgmsBillingInvoiceStatusLks.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
     */
    @Deprecated
    Page<QcEgmsBillingInvoiceStatusLk> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
     * Find all QcEgmsBillingInvoiceStatusLks matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching QcEgmsBillingInvoiceStatusLks.
     *
     * @see Pageable
     * @see Page
     */
    Page<QcEgmsBillingInvoiceStatusLk> findAll(String query, Pageable pageable);

    /**
     * Exports all QcEgmsBillingInvoiceStatusLks matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
     */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

    /**
     * Exports all QcEgmsBillingInvoiceStatusLks matching the given input query to the given exportType format.
     *
     * @param options The export options provided by the user; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @param outputStream The output stream of the file for the exported data to be written to.
     *
     * @see DataExportOptions
     * @see Pageable
     * @see OutputStream
     */
    void export(DataExportOptions options, Pageable pageable, OutputStream outputStream);

    /**
     * Retrieve the count of the QcEgmsBillingInvoiceStatusLks in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
     * @return The count of the QcEgmsBillingInvoiceStatusLk.
     */
    long count(String query);

    /**
     * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return Paginated data with included fields.
     *
     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
    Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);

    /*
     * Returns the associated qcEgmsBillingInvoiceses for given QcEgmsBillingInvoiceStatusLk id.
     *
     * @param invoicestatusid value of invoicestatusid; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated QcEgmsBillingInvoices instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<QcEgmsBillingInvoices> findAssociatedQcEgmsBillingInvoiceses(Long invoicestatusid, Pageable pageable);

}