/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingParamDataTypeLk;
import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingParamMaster;

/**
 * Service object for domain model class {@link QcEgmsBillingParamDataTypeLk}.
 */
public interface QcEgmsBillingParamDataTypeLkService {

    /**
     * Creates a new QcEgmsBillingParamDataTypeLk. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsBillingParamDataTypeLk if any.
     *
     * @param qcEgmsBillingParamDataTypeLk Details of the QcEgmsBillingParamDataTypeLk to be created; value cannot be null.
     * @return The newly created QcEgmsBillingParamDataTypeLk.
     */
    QcEgmsBillingParamDataTypeLk create(@Valid QcEgmsBillingParamDataTypeLk qcEgmsBillingParamDataTypeLk);


	/**
     * Returns QcEgmsBillingParamDataTypeLk by given id if exists.
     *
     * @param qcegmsbillingparamdatatypelkId The id of the QcEgmsBillingParamDataTypeLk to get; value cannot be null.
     * @return QcEgmsBillingParamDataTypeLk associated with the given qcegmsbillingparamdatatypelkId.
	 * @throws EntityNotFoundException If no QcEgmsBillingParamDataTypeLk is found.
     */
    QcEgmsBillingParamDataTypeLk getById(Long qcegmsbillingparamdatatypelkId);

    /**
     * Find and return the QcEgmsBillingParamDataTypeLk by given id if exists, returns null otherwise.
     *
     * @param qcegmsbillingparamdatatypelkId The id of the QcEgmsBillingParamDataTypeLk to get; value cannot be null.
     * @return QcEgmsBillingParamDataTypeLk associated with the given qcegmsbillingparamdatatypelkId.
     */
    QcEgmsBillingParamDataTypeLk findById(Long qcegmsbillingparamdatatypelkId);

	/**
     * Find and return the list of QcEgmsBillingParamDataTypeLks by given id's.
     *
     * If orderedReturn true, the return List is ordered and positional relative to the incoming ids.
     *
     * In case of unknown entities:
     *
     * If enabled, A null is inserted into the List at the proper position(s).
     * If disabled, the nulls are not put into the return List.
     *
     * @param qcegmsbillingparamdatatypelkIds The id's of the QcEgmsBillingParamDataTypeLk to get; value cannot be null.
     * @param orderedReturn Should the return List be ordered and positional in relation to the incoming ids?
     * @return QcEgmsBillingParamDataTypeLks associated with the given qcegmsbillingparamdatatypelkIds.
     */
    List<QcEgmsBillingParamDataTypeLk> findByMultipleIds(List<Long> qcegmsbillingparamdatatypelkIds, boolean orderedReturn);

    /**
     * Find and return the QcEgmsBillingParamDataTypeLk for given paramdatatypename  if exists.
     *
     * @param paramdatatypename value of paramdatatypename; value cannot be null.
     * @return QcEgmsBillingParamDataTypeLk associated with the given inputs.
     * @throws EntityNotFoundException if no matching QcEgmsBillingParamDataTypeLk found.
     */
    QcEgmsBillingParamDataTypeLk getByParamdatatypename(String paramdatatypename);

    /**
     * Updates the details of an existing QcEgmsBillingParamDataTypeLk. It replaces all fields of the existing QcEgmsBillingParamDataTypeLk with the given qcEgmsBillingParamDataTypeLk.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsBillingParamDataTypeLk if any.
     *
     * @param qcEgmsBillingParamDataTypeLk The details of the QcEgmsBillingParamDataTypeLk to be updated; value cannot be null.
     * @return The updated QcEgmsBillingParamDataTypeLk.
     * @throws EntityNotFoundException if no QcEgmsBillingParamDataTypeLk is found with given input.
     */
    QcEgmsBillingParamDataTypeLk update(@Valid QcEgmsBillingParamDataTypeLk qcEgmsBillingParamDataTypeLk);


    /**
     * Partially updates the details of an existing QcEgmsBillingParamDataTypeLk. It updates only the
     * fields of the existing QcEgmsBillingParamDataTypeLk which are passed in the qcEgmsBillingParamDataTypeLkPatch.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsBillingParamDataTypeLk if any.
     *
     * @param qcegmsbillingparamdatatypelkId The id of the QcEgmsBillingParamDataTypeLk to be deleted; value cannot be null.
     * @param qcEgmsBillingParamDataTypeLkPatch The partial data of QcEgmsBillingParamDataTypeLk which is supposed to be updated; value cannot be null.
     * @return The updated QcEgmsBillingParamDataTypeLk.
     * @throws EntityNotFoundException if no QcEgmsBillingParamDataTypeLk is found with given input.
     */
    QcEgmsBillingParamDataTypeLk partialUpdate(Long qcegmsbillingparamdatatypelkId, Map<String, Object> qcEgmsBillingParamDataTypeLkPatch);

    /**
     * Deletes an existing QcEgmsBillingParamDataTypeLk with the given id.
     *
     * @param qcegmsbillingparamdatatypelkId The id of the QcEgmsBillingParamDataTypeLk to be deleted; value cannot be null.
     * @return The deleted QcEgmsBillingParamDataTypeLk.
     * @throws EntityNotFoundException if no QcEgmsBillingParamDataTypeLk found with the given id.
     */
    QcEgmsBillingParamDataTypeLk delete(Long qcegmsbillingparamdatatypelkId);

    /**
     * Deletes an existing QcEgmsBillingParamDataTypeLk with the given object.
     *
     * @param qcEgmsBillingParamDataTypeLk The instance of the QcEgmsBillingParamDataTypeLk to be deleted; value cannot be null.
     */
    void delete(QcEgmsBillingParamDataTypeLk qcEgmsBillingParamDataTypeLk);

    /**
     * Find all QcEgmsBillingParamDataTypeLks matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
     *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
     *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching QcEgmsBillingParamDataTypeLks.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
     */
    @Deprecated
    Page<QcEgmsBillingParamDataTypeLk> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
     * Find all QcEgmsBillingParamDataTypeLks matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching QcEgmsBillingParamDataTypeLks.
     *
     * @see Pageable
     * @see Page
     */
    Page<QcEgmsBillingParamDataTypeLk> findAll(String query, Pageable pageable);

    /**
     * Exports all QcEgmsBillingParamDataTypeLks matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
     */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

    /**
     * Exports all QcEgmsBillingParamDataTypeLks matching the given input query to the given exportType format.
     *
     * @param options The export options provided by the user; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @param outputStream The output stream of the file for the exported data to be written to.
     *
     * @see DataExportOptions
     * @see Pageable
     * @see OutputStream
     */
    void export(DataExportOptions options, Pageable pageable, OutputStream outputStream);

    /**
     * Retrieve the count of the QcEgmsBillingParamDataTypeLks in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
     * @return The count of the QcEgmsBillingParamDataTypeLk.
     */
    long count(String query);

    /**
     * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return Paginated data with included fields.
     *
     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
    Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);

    /*
     * Returns the associated qcEgmsBillingParamMasters for given QcEgmsBillingParamDataTypeLk id.
     *
     * @param paramdatatypeid value of paramdatatypeid; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated QcEgmsBillingParamMaster instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<QcEgmsBillingParamMaster> findAssociatedQcEgmsBillingParamMasters(Long paramdatatypeid, Pageable pageable);

}