/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.wavemaker.commons.InvalidInputException;
import com.wavemaker.commons.MessageResource;
import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingGroupTypeLk;
import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingGroups;


/**
 * ServiceImpl object for domain model class QcEgmsBillingGroupTypeLk.
 *
 * @see QcEgmsBillingGroupTypeLk
 */
@Service("WM_Billing_Admin.QcEgmsBillingGroupTypeLkService")
@Validated
public class QcEgmsBillingGroupTypeLkServiceImpl implements QcEgmsBillingGroupTypeLkService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QcEgmsBillingGroupTypeLkServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("WM_Billing_Admin.QcEgmsBillingGroupsService")
    private QcEgmsBillingGroupsService qcEgmsBillingGroupsService;

    @Autowired
    @Qualifier("WM_Billing_Admin.QcEgmsBillingGroupTypeLkDao")
    private WMGenericDao<QcEgmsBillingGroupTypeLk, Long> wmGenericDao;

    @Autowired
    @Qualifier("wmAppObjectMapper")
    private ObjectMapper objectMapper;


    public void setWMGenericDao(WMGenericDao<QcEgmsBillingGroupTypeLk, Long> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupTypeLk create(QcEgmsBillingGroupTypeLk qcEgmsBillingGroupTypeLk) {
        LOGGER.debug("Creating a new QcEgmsBillingGroupTypeLk with information: {}", qcEgmsBillingGroupTypeLk);

        QcEgmsBillingGroupTypeLk qcEgmsBillingGroupTypeLkCreated = this.wmGenericDao.create(qcEgmsBillingGroupTypeLk);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(qcEgmsBillingGroupTypeLkCreated);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupTypeLk getById(Long qcegmsbillinggrouptypelkId) {
        LOGGER.debug("Finding QcEgmsBillingGroupTypeLk by id: {}", qcegmsbillinggrouptypelkId);
        return this.wmGenericDao.findById(qcegmsbillinggrouptypelkId);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupTypeLk findById(Long qcegmsbillinggrouptypelkId) {
        LOGGER.debug("Finding QcEgmsBillingGroupTypeLk by id: {}", qcegmsbillinggrouptypelkId);
        try {
            return this.wmGenericDao.findById(qcegmsbillinggrouptypelkId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No QcEgmsBillingGroupTypeLk found with id: {}", qcegmsbillinggrouptypelkId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public List<QcEgmsBillingGroupTypeLk> findByMultipleIds(List<Long> qcegmsbillinggrouptypelkIds, boolean orderedReturn) {
        LOGGER.debug("Finding QcEgmsBillingGroupTypeLks by ids: {}", qcegmsbillinggrouptypelkIds);

        return this.wmGenericDao.findByMultipleIds(qcegmsbillinggrouptypelkIds, orderedReturn);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupTypeLk getByBillinggrouptypename(String billinggrouptypename) {
        Map<String, Object> billinggrouptypenameMap = new HashMap<>();
        billinggrouptypenameMap.put("billinggrouptypename", billinggrouptypename);

        LOGGER.debug("Finding QcEgmsBillingGroupTypeLk by unique keys: {}", billinggrouptypenameMap);
        return this.wmGenericDao.findByUniqueKey(billinggrouptypenameMap);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupTypeLk update(QcEgmsBillingGroupTypeLk qcEgmsBillingGroupTypeLk) {
        LOGGER.debug("Updating QcEgmsBillingGroupTypeLk with information: {}", qcEgmsBillingGroupTypeLk);

        this.wmGenericDao.update(qcEgmsBillingGroupTypeLk);
        this.wmGenericDao.refresh(qcEgmsBillingGroupTypeLk);

        return qcEgmsBillingGroupTypeLk;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupTypeLk partialUpdate(Long qcegmsbillinggrouptypelkId, Map<String, Object>qcEgmsBillingGroupTypeLkPatch) {
        LOGGER.debug("Partially Updating the QcEgmsBillingGroupTypeLk with id: {}", qcegmsbillinggrouptypelkId);

        QcEgmsBillingGroupTypeLk qcEgmsBillingGroupTypeLk = getById(qcegmsbillinggrouptypelkId);

        try {
            ObjectReader qcEgmsBillingGroupTypeLkReader = this.objectMapper.reader().forType(QcEgmsBillingGroupTypeLk.class).withValueToUpdate(qcEgmsBillingGroupTypeLk);
            qcEgmsBillingGroupTypeLk = qcEgmsBillingGroupTypeLkReader.readValue(this.objectMapper.writeValueAsString(qcEgmsBillingGroupTypeLkPatch));
        } catch (IOException ex) {
            LOGGER.debug("There was a problem in applying the patch: {}", qcEgmsBillingGroupTypeLkPatch, ex);
            throw new InvalidInputException("Could not apply patch",ex);
        }

        qcEgmsBillingGroupTypeLk = update(qcEgmsBillingGroupTypeLk);

        return qcEgmsBillingGroupTypeLk;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupTypeLk delete(Long qcegmsbillinggrouptypelkId) {
        LOGGER.debug("Deleting QcEgmsBillingGroupTypeLk with id: {}", qcegmsbillinggrouptypelkId);
        QcEgmsBillingGroupTypeLk deleted = this.wmGenericDao.findById(qcegmsbillinggrouptypelkId);
        if (deleted == null) {
            LOGGER.debug("No QcEgmsBillingGroupTypeLk found with id: {}", qcegmsbillinggrouptypelkId);
            throw new EntityNotFoundException(MessageResource.create("com.wavemaker.runtime.entity.not.found"), QcEgmsBillingGroupTypeLk.class.getSimpleName(), qcegmsbillinggrouptypelkId);
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public void delete(QcEgmsBillingGroupTypeLk qcEgmsBillingGroupTypeLk) {
        LOGGER.debug("Deleting QcEgmsBillingGroupTypeLk with {}", qcEgmsBillingGroupTypeLk);
        this.wmGenericDao.delete(qcEgmsBillingGroupTypeLk);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingGroupTypeLk> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingGroupTypeLks");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingGroupTypeLk> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingGroupTypeLks");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingGroupTypeLk to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingGroupTypeLk to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingGroups> findAssociatedQcEgmsBillingGroupses(Long billinggrouptypeid, Pageable pageable) {
        LOGGER.debug("Fetching all associated qcEgmsBillingGroupses");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("qcEgmsBillingGroupTypeLk.billinggrouptypeid = '" + billinggrouptypeid + "'");

        return qcEgmsBillingGroupsService.findAll(queryBuilder.toString(), pageable);
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service QcEgmsBillingGroupsService instance
     */
    protected void setQcEgmsBillingGroupsService(QcEgmsBillingGroupsService service) {
        this.qcEgmsBillingGroupsService = service;
    }

}