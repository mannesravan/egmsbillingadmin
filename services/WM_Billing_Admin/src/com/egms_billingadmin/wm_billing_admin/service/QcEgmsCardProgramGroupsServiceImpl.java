/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.wavemaker.commons.InvalidInputException;
import com.wavemaker.commons.MessageResource;
import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsCardProgramGroups;


/**
 * ServiceImpl object for domain model class QcEgmsCardProgramGroups.
 *
 * @see QcEgmsCardProgramGroups
 */
@Service("WM_Billing_Admin.QcEgmsCardProgramGroupsService")
@Validated
public class QcEgmsCardProgramGroupsServiceImpl implements QcEgmsCardProgramGroupsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QcEgmsCardProgramGroupsServiceImpl.class);


    @Autowired
    @Qualifier("WM_Billing_Admin.QcEgmsCardProgramGroupsDao")
    private WMGenericDao<QcEgmsCardProgramGroups, Long> wmGenericDao;

    @Autowired
    @Qualifier("wmAppObjectMapper")
    private ObjectMapper objectMapper;


    public void setWMGenericDao(WMGenericDao<QcEgmsCardProgramGroups, Long> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsCardProgramGroups create(QcEgmsCardProgramGroups qcEgmsCardProgramGroups) {
        LOGGER.debug("Creating a new QcEgmsCardProgramGroups with information: {}", qcEgmsCardProgramGroups);

        QcEgmsCardProgramGroups qcEgmsCardProgramGroupsCreated = this.wmGenericDao.create(qcEgmsCardProgramGroups);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(qcEgmsCardProgramGroupsCreated);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsCardProgramGroups getById(Long qcegmscardprogramgroupsId) {
        LOGGER.debug("Finding QcEgmsCardProgramGroups by id: {}", qcegmscardprogramgroupsId);
        return this.wmGenericDao.findById(qcegmscardprogramgroupsId);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsCardProgramGroups findById(Long qcegmscardprogramgroupsId) {
        LOGGER.debug("Finding QcEgmsCardProgramGroups by id: {}", qcegmscardprogramgroupsId);
        try {
            return this.wmGenericDao.findById(qcegmscardprogramgroupsId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No QcEgmsCardProgramGroups found with id: {}", qcegmscardprogramgroupsId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public List<QcEgmsCardProgramGroups> findByMultipleIds(List<Long> qcegmscardprogramgroupsIds, boolean orderedReturn) {
        LOGGER.debug("Finding QcEgmsCardProgramGroups by ids: {}", qcegmscardprogramgroupsIds);

        return this.wmGenericDao.findByMultipleIds(qcegmscardprogramgroupsIds, orderedReturn);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsCardProgramGroups getByCardprogramdescAndIssuerid(String cardprogramdesc, long issuerid) {
        Map<String, Object> cardprogramdescAndIssueridMap = new HashMap<>();
        cardprogramdescAndIssueridMap.put("cardprogramdesc", cardprogramdesc);
        cardprogramdescAndIssueridMap.put("issuerid", issuerid);

        LOGGER.debug("Finding QcEgmsCardProgramGroups by unique keys: {}", cardprogramdescAndIssueridMap);
        return this.wmGenericDao.findByUniqueKey(cardprogramdescAndIssueridMap);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsCardProgramGroups update(QcEgmsCardProgramGroups qcEgmsCardProgramGroups) {
        LOGGER.debug("Updating QcEgmsCardProgramGroups with information: {}", qcEgmsCardProgramGroups);

        this.wmGenericDao.update(qcEgmsCardProgramGroups);
        this.wmGenericDao.refresh(qcEgmsCardProgramGroups);

        return qcEgmsCardProgramGroups;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsCardProgramGroups partialUpdate(Long qcegmscardprogramgroupsId, Map<String, Object>qcEgmsCardProgramGroupsPatch) {
        LOGGER.debug("Partially Updating the QcEgmsCardProgramGroups with id: {}", qcegmscardprogramgroupsId);

        QcEgmsCardProgramGroups qcEgmsCardProgramGroups = getById(qcegmscardprogramgroupsId);

        try {
            ObjectReader qcEgmsCardProgramGroupsReader = this.objectMapper.reader().forType(QcEgmsCardProgramGroups.class).withValueToUpdate(qcEgmsCardProgramGroups);
            qcEgmsCardProgramGroups = qcEgmsCardProgramGroupsReader.readValue(this.objectMapper.writeValueAsString(qcEgmsCardProgramGroupsPatch));
        } catch (IOException ex) {
            LOGGER.debug("There was a problem in applying the patch: {}", qcEgmsCardProgramGroupsPatch, ex);
            throw new InvalidInputException("Could not apply patch",ex);
        }

        qcEgmsCardProgramGroups = update(qcEgmsCardProgramGroups);

        return qcEgmsCardProgramGroups;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsCardProgramGroups delete(Long qcegmscardprogramgroupsId) {
        LOGGER.debug("Deleting QcEgmsCardProgramGroups with id: {}", qcegmscardprogramgroupsId);
        QcEgmsCardProgramGroups deleted = this.wmGenericDao.findById(qcegmscardprogramgroupsId);
        if (deleted == null) {
            LOGGER.debug("No QcEgmsCardProgramGroups found with id: {}", qcegmscardprogramgroupsId);
            throw new EntityNotFoundException(MessageResource.create("com.wavemaker.runtime.entity.not.found"), QcEgmsCardProgramGroups.class.getSimpleName(), qcegmscardprogramgroupsId);
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public void delete(QcEgmsCardProgramGroups qcEgmsCardProgramGroups) {
        LOGGER.debug("Deleting QcEgmsCardProgramGroups with {}", qcEgmsCardProgramGroups);
        this.wmGenericDao.delete(qcEgmsCardProgramGroups);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsCardProgramGroups> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsCardProgramGroups");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsCardProgramGroups> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsCardProgramGroups");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsCardProgramGroups to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsCardProgramGroups to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}