/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.wavemaker.commons.InvalidInputException;
import com.wavemaker.commons.MessageResource;
import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingCardProgramGroupMap;


/**
 * ServiceImpl object for domain model class QcEgmsBillingCardProgramGroupMap.
 *
 * @see QcEgmsBillingCardProgramGroupMap
 */
@Service("WM_Billing_Admin.QcEgmsBillingCardProgramGroupMapService")
@Validated
public class QcEgmsBillingCardProgramGroupMapServiceImpl implements QcEgmsBillingCardProgramGroupMapService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QcEgmsBillingCardProgramGroupMapServiceImpl.class);


    @Autowired
    @Qualifier("WM_Billing_Admin.QcEgmsBillingCardProgramGroupMapDao")
    private WMGenericDao<QcEgmsBillingCardProgramGroupMap, Long> wmGenericDao;

    @Autowired
    @Qualifier("wmAppObjectMapper")
    private ObjectMapper objectMapper;


    public void setWMGenericDao(WMGenericDao<QcEgmsBillingCardProgramGroupMap, Long> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingCardProgramGroupMap create(QcEgmsBillingCardProgramGroupMap qcEgmsBillingCardProgramGroupMap) {
        LOGGER.debug("Creating a new QcEgmsBillingCardProgramGroupMap with information: {}", qcEgmsBillingCardProgramGroupMap);

        QcEgmsBillingCardProgramGroupMap qcEgmsBillingCardProgramGroupMapCreated = this.wmGenericDao.create(qcEgmsBillingCardProgramGroupMap);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(qcEgmsBillingCardProgramGroupMapCreated);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingCardProgramGroupMap getById(Long qcegmsbillingcardprogramgroupmapId) {
        LOGGER.debug("Finding QcEgmsBillingCardProgramGroupMap by id: {}", qcegmsbillingcardprogramgroupmapId);
        return this.wmGenericDao.findById(qcegmsbillingcardprogramgroupmapId);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingCardProgramGroupMap findById(Long qcegmsbillingcardprogramgroupmapId) {
        LOGGER.debug("Finding QcEgmsBillingCardProgramGroupMap by id: {}", qcegmsbillingcardprogramgroupmapId);
        try {
            return this.wmGenericDao.findById(qcegmsbillingcardprogramgroupmapId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No QcEgmsBillingCardProgramGroupMap found with id: {}", qcegmsbillingcardprogramgroupmapId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public List<QcEgmsBillingCardProgramGroupMap> findByMultipleIds(List<Long> qcegmsbillingcardprogramgroupmapIds, boolean orderedReturn) {
        LOGGER.debug("Finding QcEgmsBillingCardProgramGroupMaps by ids: {}", qcegmsbillingcardprogramgroupmapIds);

        return this.wmGenericDao.findByMultipleIds(qcegmsbillingcardprogramgroupmapIds, orderedReturn);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingCardProgramGroupMap getByCardprogramgroupid(long cardprogramgroupid) {
        Map<String, Object> cardprogramgroupidMap = new HashMap<>();
        cardprogramgroupidMap.put("cardprogramgroupid", cardprogramgroupid);

        LOGGER.debug("Finding QcEgmsBillingCardProgramGroupMap by unique keys: {}", cardprogramgroupidMap);
        return this.wmGenericDao.findByUniqueKey(cardprogramgroupidMap);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingCardProgramGroupMap update(QcEgmsBillingCardProgramGroupMap qcEgmsBillingCardProgramGroupMap) {
        LOGGER.debug("Updating QcEgmsBillingCardProgramGroupMap with information: {}", qcEgmsBillingCardProgramGroupMap);

        this.wmGenericDao.update(qcEgmsBillingCardProgramGroupMap);
        this.wmGenericDao.refresh(qcEgmsBillingCardProgramGroupMap);

        return qcEgmsBillingCardProgramGroupMap;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingCardProgramGroupMap partialUpdate(Long qcegmsbillingcardprogramgroupmapId, Map<String, Object>qcEgmsBillingCardProgramGroupMapPatch) {
        LOGGER.debug("Partially Updating the QcEgmsBillingCardProgramGroupMap with id: {}", qcegmsbillingcardprogramgroupmapId);

        QcEgmsBillingCardProgramGroupMap qcEgmsBillingCardProgramGroupMap = getById(qcegmsbillingcardprogramgroupmapId);

        try {
            ObjectReader qcEgmsBillingCardProgramGroupMapReader = this.objectMapper.reader().forType(QcEgmsBillingCardProgramGroupMap.class).withValueToUpdate(qcEgmsBillingCardProgramGroupMap);
            qcEgmsBillingCardProgramGroupMap = qcEgmsBillingCardProgramGroupMapReader.readValue(this.objectMapper.writeValueAsString(qcEgmsBillingCardProgramGroupMapPatch));
        } catch (IOException ex) {
            LOGGER.debug("There was a problem in applying the patch: {}", qcEgmsBillingCardProgramGroupMapPatch, ex);
            throw new InvalidInputException("Could not apply patch",ex);
        }

        qcEgmsBillingCardProgramGroupMap = update(qcEgmsBillingCardProgramGroupMap);

        return qcEgmsBillingCardProgramGroupMap;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingCardProgramGroupMap delete(Long qcegmsbillingcardprogramgroupmapId) {
        LOGGER.debug("Deleting QcEgmsBillingCardProgramGroupMap with id: {}", qcegmsbillingcardprogramgroupmapId);
        QcEgmsBillingCardProgramGroupMap deleted = this.wmGenericDao.findById(qcegmsbillingcardprogramgroupmapId);
        if (deleted == null) {
            LOGGER.debug("No QcEgmsBillingCardProgramGroupMap found with id: {}", qcegmsbillingcardprogramgroupmapId);
            throw new EntityNotFoundException(MessageResource.create("com.wavemaker.runtime.entity.not.found"), QcEgmsBillingCardProgramGroupMap.class.getSimpleName(), qcegmsbillingcardprogramgroupmapId);
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public void delete(QcEgmsBillingCardProgramGroupMap qcEgmsBillingCardProgramGroupMap) {
        LOGGER.debug("Deleting QcEgmsBillingCardProgramGroupMap with {}", qcEgmsBillingCardProgramGroupMap);
        this.wmGenericDao.delete(qcEgmsBillingCardProgramGroupMap);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingCardProgramGroupMap> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingCardProgramGroupMaps");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingCardProgramGroupMap> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingCardProgramGroupMaps");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingCardProgramGroupMap to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingCardProgramGroupMap to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}