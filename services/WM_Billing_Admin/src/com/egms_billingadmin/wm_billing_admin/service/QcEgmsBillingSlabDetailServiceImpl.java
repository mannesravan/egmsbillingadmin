/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.wavemaker.commons.InvalidInputException;
import com.wavemaker.commons.MessageResource;
import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingSlabDetail;
import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingSlabDetailId;


/**
 * ServiceImpl object for domain model class QcEgmsBillingSlabDetail.
 *
 * @see QcEgmsBillingSlabDetail
 */
@Service("WM_Billing_Admin.QcEgmsBillingSlabDetailService")
@Validated
public class QcEgmsBillingSlabDetailServiceImpl implements QcEgmsBillingSlabDetailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QcEgmsBillingSlabDetailServiceImpl.class);


    @Autowired
    @Qualifier("WM_Billing_Admin.QcEgmsBillingSlabDetailDao")
    private WMGenericDao<QcEgmsBillingSlabDetail, QcEgmsBillingSlabDetailId> wmGenericDao;

    @Autowired
    @Qualifier("wmAppObjectMapper")
    private ObjectMapper objectMapper;


    public void setWMGenericDao(WMGenericDao<QcEgmsBillingSlabDetail, QcEgmsBillingSlabDetailId> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingSlabDetail create(QcEgmsBillingSlabDetail qcEgmsBillingSlabDetail) {
        LOGGER.debug("Creating a new QcEgmsBillingSlabDetail with information: {}", qcEgmsBillingSlabDetail);

        QcEgmsBillingSlabDetail qcEgmsBillingSlabDetailCreated = this.wmGenericDao.create(qcEgmsBillingSlabDetail);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(qcEgmsBillingSlabDetailCreated);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingSlabDetail getById(QcEgmsBillingSlabDetailId qcegmsbillingslabdetailId) {
        LOGGER.debug("Finding QcEgmsBillingSlabDetail by id: {}", qcegmsbillingslabdetailId);
        return this.wmGenericDao.findById(qcegmsbillingslabdetailId);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingSlabDetail findById(QcEgmsBillingSlabDetailId qcegmsbillingslabdetailId) {
        LOGGER.debug("Finding QcEgmsBillingSlabDetail by id: {}", qcegmsbillingslabdetailId);
        try {
            return this.wmGenericDao.findById(qcegmsbillingslabdetailId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No QcEgmsBillingSlabDetail found with id: {}", qcegmsbillingslabdetailId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public List<QcEgmsBillingSlabDetail> findByMultipleIds(List<QcEgmsBillingSlabDetailId> qcegmsbillingslabdetailIds, boolean orderedReturn) {
        LOGGER.debug("Finding QcEgmsBillingSlabDetails by ids: {}", qcegmsbillingslabdetailIds);

        return this.wmGenericDao.findByMultipleIds(qcegmsbillingslabdetailIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingSlabDetail update(QcEgmsBillingSlabDetail qcEgmsBillingSlabDetail) {
        LOGGER.debug("Updating QcEgmsBillingSlabDetail with information: {}", qcEgmsBillingSlabDetail);

        this.wmGenericDao.update(qcEgmsBillingSlabDetail);
        this.wmGenericDao.refresh(qcEgmsBillingSlabDetail);

        return qcEgmsBillingSlabDetail;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingSlabDetail partialUpdate(QcEgmsBillingSlabDetailId qcegmsbillingslabdetailId, Map<String, Object>qcEgmsBillingSlabDetailPatch) {
        LOGGER.debug("Partially Updating the QcEgmsBillingSlabDetail with id: {}", qcegmsbillingslabdetailId);

        QcEgmsBillingSlabDetail qcEgmsBillingSlabDetail = getById(qcegmsbillingslabdetailId);

        try {
            ObjectReader qcEgmsBillingSlabDetailReader = this.objectMapper.reader().forType(QcEgmsBillingSlabDetail.class).withValueToUpdate(qcEgmsBillingSlabDetail);
            qcEgmsBillingSlabDetail = qcEgmsBillingSlabDetailReader.readValue(this.objectMapper.writeValueAsString(qcEgmsBillingSlabDetailPatch));
        } catch (IOException ex) {
            LOGGER.debug("There was a problem in applying the patch: {}", qcEgmsBillingSlabDetailPatch, ex);
            throw new InvalidInputException("Could not apply patch",ex);
        }

        qcEgmsBillingSlabDetail = update(qcEgmsBillingSlabDetail);

        return qcEgmsBillingSlabDetail;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingSlabDetail delete(QcEgmsBillingSlabDetailId qcegmsbillingslabdetailId) {
        LOGGER.debug("Deleting QcEgmsBillingSlabDetail with id: {}", qcegmsbillingslabdetailId);
        QcEgmsBillingSlabDetail deleted = this.wmGenericDao.findById(qcegmsbillingslabdetailId);
        if (deleted == null) {
            LOGGER.debug("No QcEgmsBillingSlabDetail found with id: {}", qcegmsbillingslabdetailId);
            throw new EntityNotFoundException(MessageResource.create("com.wavemaker.runtime.entity.not.found"), QcEgmsBillingSlabDetail.class.getSimpleName(), qcegmsbillingslabdetailId);
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public void delete(QcEgmsBillingSlabDetail qcEgmsBillingSlabDetail) {
        LOGGER.debug("Deleting QcEgmsBillingSlabDetail with {}", qcEgmsBillingSlabDetail);
        this.wmGenericDao.delete(qcEgmsBillingSlabDetail);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingSlabDetail> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingSlabDetails");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingSlabDetail> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingSlabDetails");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingSlabDetail to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingSlabDetail to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}