/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.wavemaker.commons.InvalidInputException;
import com.wavemaker.commons.MessageResource;
import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingGroupDefaultConfig;


/**
 * ServiceImpl object for domain model class QcEgmsBillingGroupDefaultConfig.
 *
 * @see QcEgmsBillingGroupDefaultConfig
 */
@Service("WM_Billing_Admin.QcEgmsBillingGroupDefaultConfigService")
@Validated
public class QcEgmsBillingGroupDefaultConfigServiceImpl implements QcEgmsBillingGroupDefaultConfigService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QcEgmsBillingGroupDefaultConfigServiceImpl.class);


    @Autowired
    @Qualifier("WM_Billing_Admin.QcEgmsBillingGroupDefaultConfigDao")
    private WMGenericDao<QcEgmsBillingGroupDefaultConfig, Long> wmGenericDao;

    @Autowired
    @Qualifier("wmAppObjectMapper")
    private ObjectMapper objectMapper;


    public void setWMGenericDao(WMGenericDao<QcEgmsBillingGroupDefaultConfig, Long> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupDefaultConfig create(QcEgmsBillingGroupDefaultConfig qcEgmsBillingGroupDefaultConfig) {
        LOGGER.debug("Creating a new QcEgmsBillingGroupDefaultConfig with information: {}", qcEgmsBillingGroupDefaultConfig);

        QcEgmsBillingGroupDefaultConfig qcEgmsBillingGroupDefaultConfigCreated = this.wmGenericDao.create(qcEgmsBillingGroupDefaultConfig);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(qcEgmsBillingGroupDefaultConfigCreated);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupDefaultConfig getById(Long qcegmsbillinggroupdefaultconfigId) {
        LOGGER.debug("Finding QcEgmsBillingGroupDefaultConfig by id: {}", qcegmsbillinggroupdefaultconfigId);
        return this.wmGenericDao.findById(qcegmsbillinggroupdefaultconfigId);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupDefaultConfig findById(Long qcegmsbillinggroupdefaultconfigId) {
        LOGGER.debug("Finding QcEgmsBillingGroupDefaultConfig by id: {}", qcegmsbillinggroupdefaultconfigId);
        try {
            return this.wmGenericDao.findById(qcegmsbillinggroupdefaultconfigId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No QcEgmsBillingGroupDefaultConfig found with id: {}", qcegmsbillinggroupdefaultconfigId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public List<QcEgmsBillingGroupDefaultConfig> findByMultipleIds(List<Long> qcegmsbillinggroupdefaultconfigIds, boolean orderedReturn) {
        LOGGER.debug("Finding QcEgmsBillingGroupDefaultConfigs by ids: {}", qcegmsbillinggroupdefaultconfigIds);

        return this.wmGenericDao.findByMultipleIds(qcegmsbillinggroupdefaultconfigIds, orderedReturn);
    }


    @Transactional(rollbackFor = EntityNotFoundException.class, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupDefaultConfig update(QcEgmsBillingGroupDefaultConfig qcEgmsBillingGroupDefaultConfig) {
        LOGGER.debug("Updating QcEgmsBillingGroupDefaultConfig with information: {}", qcEgmsBillingGroupDefaultConfig);

        this.wmGenericDao.update(qcEgmsBillingGroupDefaultConfig);
        this.wmGenericDao.refresh(qcEgmsBillingGroupDefaultConfig);

        return qcEgmsBillingGroupDefaultConfig;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupDefaultConfig partialUpdate(Long qcegmsbillinggroupdefaultconfigId, Map<String, Object>qcEgmsBillingGroupDefaultConfigPatch) {
        LOGGER.debug("Partially Updating the QcEgmsBillingGroupDefaultConfig with id: {}", qcegmsbillinggroupdefaultconfigId);

        QcEgmsBillingGroupDefaultConfig qcEgmsBillingGroupDefaultConfig = getById(qcegmsbillinggroupdefaultconfigId);

        try {
            ObjectReader qcEgmsBillingGroupDefaultConfigReader = this.objectMapper.reader().forType(QcEgmsBillingGroupDefaultConfig.class).withValueToUpdate(qcEgmsBillingGroupDefaultConfig);
            qcEgmsBillingGroupDefaultConfig = qcEgmsBillingGroupDefaultConfigReader.readValue(this.objectMapper.writeValueAsString(qcEgmsBillingGroupDefaultConfigPatch));
        } catch (IOException ex) {
            LOGGER.debug("There was a problem in applying the patch: {}", qcEgmsBillingGroupDefaultConfigPatch, ex);
            throw new InvalidInputException("Could not apply patch",ex);
        }

        qcEgmsBillingGroupDefaultConfig = update(qcEgmsBillingGroupDefaultConfig);

        return qcEgmsBillingGroupDefaultConfig;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupDefaultConfig delete(Long qcegmsbillinggroupdefaultconfigId) {
        LOGGER.debug("Deleting QcEgmsBillingGroupDefaultConfig with id: {}", qcegmsbillinggroupdefaultconfigId);
        QcEgmsBillingGroupDefaultConfig deleted = this.wmGenericDao.findById(qcegmsbillinggroupdefaultconfigId);
        if (deleted == null) {
            LOGGER.debug("No QcEgmsBillingGroupDefaultConfig found with id: {}", qcegmsbillinggroupdefaultconfigId);
            throw new EntityNotFoundException(MessageResource.create("com.wavemaker.runtime.entity.not.found"), QcEgmsBillingGroupDefaultConfig.class.getSimpleName(), qcegmsbillinggroupdefaultconfigId);
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public void delete(QcEgmsBillingGroupDefaultConfig qcEgmsBillingGroupDefaultConfig) {
        LOGGER.debug("Deleting QcEgmsBillingGroupDefaultConfig with {}", qcEgmsBillingGroupDefaultConfig);
        this.wmGenericDao.delete(qcEgmsBillingGroupDefaultConfig);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingGroupDefaultConfig> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingGroupDefaultConfigs");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingGroupDefaultConfig> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingGroupDefaultConfigs");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingGroupDefaultConfig to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingGroupDefaultConfig to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }



}