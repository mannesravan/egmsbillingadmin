/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.wavemaker.commons.InvalidInputException;
import com.wavemaker.commons.MessageResource;
import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingGroupStatusLk;
import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingGroups;


/**
 * ServiceImpl object for domain model class QcEgmsBillingGroupStatusLk.
 *
 * @see QcEgmsBillingGroupStatusLk
 */
@Service("WM_Billing_Admin.QcEgmsBillingGroupStatusLkService")
@Validated
public class QcEgmsBillingGroupStatusLkServiceImpl implements QcEgmsBillingGroupStatusLkService {

    private static final Logger LOGGER = LoggerFactory.getLogger(QcEgmsBillingGroupStatusLkServiceImpl.class);

    @Lazy
    @Autowired
    @Qualifier("WM_Billing_Admin.QcEgmsBillingGroupsService")
    private QcEgmsBillingGroupsService qcEgmsBillingGroupsService;

    @Autowired
    @Qualifier("WM_Billing_Admin.QcEgmsBillingGroupStatusLkDao")
    private WMGenericDao<QcEgmsBillingGroupStatusLk, Long> wmGenericDao;

    @Autowired
    @Qualifier("wmAppObjectMapper")
    private ObjectMapper objectMapper;


    public void setWMGenericDao(WMGenericDao<QcEgmsBillingGroupStatusLk, Long> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupStatusLk create(QcEgmsBillingGroupStatusLk qcEgmsBillingGroupStatusLk) {
        LOGGER.debug("Creating a new QcEgmsBillingGroupStatusLk with information: {}", qcEgmsBillingGroupStatusLk);

        QcEgmsBillingGroupStatusLk qcEgmsBillingGroupStatusLkCreated = this.wmGenericDao.create(qcEgmsBillingGroupStatusLk);
        // reloading object from database to get database defined & server defined values.
        return this.wmGenericDao.refresh(qcEgmsBillingGroupStatusLkCreated);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupStatusLk getById(Long qcegmsbillinggroupstatuslkId) {
        LOGGER.debug("Finding QcEgmsBillingGroupStatusLk by id: {}", qcegmsbillinggroupstatuslkId);
        return this.wmGenericDao.findById(qcegmsbillinggroupstatuslkId);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupStatusLk findById(Long qcegmsbillinggroupstatuslkId) {
        LOGGER.debug("Finding QcEgmsBillingGroupStatusLk by id: {}", qcegmsbillinggroupstatuslkId);
        try {
            return this.wmGenericDao.findById(qcegmsbillinggroupstatuslkId);
        } catch (EntityNotFoundException ex) {
            LOGGER.debug("No QcEgmsBillingGroupStatusLk found with id: {}", qcegmsbillinggroupstatuslkId, ex);
            return null;
        }
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public List<QcEgmsBillingGroupStatusLk> findByMultipleIds(List<Long> qcegmsbillinggroupstatuslkIds, boolean orderedReturn) {
        LOGGER.debug("Finding QcEgmsBillingGroupStatusLks by ids: {}", qcegmsbillinggroupstatuslkIds);

        return this.wmGenericDao.findByMultipleIds(qcegmsbillinggroupstatuslkIds, orderedReturn);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupStatusLk getByBillinggroupstatusname(String billinggroupstatusname) {
        Map<String, Object> billinggroupstatusnameMap = new HashMap<>();
        billinggroupstatusnameMap.put("billinggroupstatusname", billinggroupstatusname);

        LOGGER.debug("Finding QcEgmsBillingGroupStatusLk by unique keys: {}", billinggroupstatusnameMap);
        return this.wmGenericDao.findByUniqueKey(billinggroupstatusnameMap);
    }

    @Transactional(rollbackFor = EntityNotFoundException.class, value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupStatusLk update(QcEgmsBillingGroupStatusLk qcEgmsBillingGroupStatusLk) {
        LOGGER.debug("Updating QcEgmsBillingGroupStatusLk with information: {}", qcEgmsBillingGroupStatusLk);

        this.wmGenericDao.update(qcEgmsBillingGroupStatusLk);
        this.wmGenericDao.refresh(qcEgmsBillingGroupStatusLk);

        return qcEgmsBillingGroupStatusLk;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupStatusLk partialUpdate(Long qcegmsbillinggroupstatuslkId, Map<String, Object>qcEgmsBillingGroupStatusLkPatch) {
        LOGGER.debug("Partially Updating the QcEgmsBillingGroupStatusLk with id: {}", qcegmsbillinggroupstatuslkId);

        QcEgmsBillingGroupStatusLk qcEgmsBillingGroupStatusLk = getById(qcegmsbillinggroupstatuslkId);

        try {
            ObjectReader qcEgmsBillingGroupStatusLkReader = this.objectMapper.reader().forType(QcEgmsBillingGroupStatusLk.class).withValueToUpdate(qcEgmsBillingGroupStatusLk);
            qcEgmsBillingGroupStatusLk = qcEgmsBillingGroupStatusLkReader.readValue(this.objectMapper.writeValueAsString(qcEgmsBillingGroupStatusLkPatch));
        } catch (IOException ex) {
            LOGGER.debug("There was a problem in applying the patch: {}", qcEgmsBillingGroupStatusLkPatch, ex);
            throw new InvalidInputException("Could not apply patch",ex);
        }

        qcEgmsBillingGroupStatusLk = update(qcEgmsBillingGroupStatusLk);

        return qcEgmsBillingGroupStatusLk;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public QcEgmsBillingGroupStatusLk delete(Long qcegmsbillinggroupstatuslkId) {
        LOGGER.debug("Deleting QcEgmsBillingGroupStatusLk with id: {}", qcegmsbillinggroupstatuslkId);
        QcEgmsBillingGroupStatusLk deleted = this.wmGenericDao.findById(qcegmsbillinggroupstatuslkId);
        if (deleted == null) {
            LOGGER.debug("No QcEgmsBillingGroupStatusLk found with id: {}", qcegmsbillinggroupstatuslkId);
            throw new EntityNotFoundException(MessageResource.create("com.wavemaker.runtime.entity.not.found"), QcEgmsBillingGroupStatusLk.class.getSimpleName(), qcegmsbillinggroupstatuslkId);
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

    @Transactional(value = "WM_Billing_AdminTransactionManager")
    @Override
    public void delete(QcEgmsBillingGroupStatusLk qcEgmsBillingGroupStatusLk) {
        LOGGER.debug("Deleting QcEgmsBillingGroupStatusLk with {}", qcEgmsBillingGroupStatusLk);
        this.wmGenericDao.delete(qcEgmsBillingGroupStatusLk);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingGroupStatusLk> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingGroupStatusLks");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingGroupStatusLk> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all QcEgmsBillingGroupStatusLks");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingGroupStatusLk to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager", timeout = 300)
    @Override
    public void export(DataExportOptions options, Pageable pageable, OutputStream outputStream) {
        LOGGER.debug("exporting data in the service WM_Billing_Admin for table QcEgmsBillingGroupStatusLk to {} format", options.getExportType());
        this.wmGenericDao.export(options, pageable, outputStream);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "WM_Billing_AdminTransactionManager")
    @Override
    public Page<QcEgmsBillingGroups> findAssociatedQcEgmsBillingGroupses(Long billinggroupstatusid, Pageable pageable) {
        LOGGER.debug("Fetching all associated qcEgmsBillingGroupses");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("qcEgmsBillingGroupStatusLk.billinggroupstatusid = '" + billinggroupstatusid + "'");

        return qcEgmsBillingGroupsService.findAll(queryBuilder.toString(), pageable);
    }

    /**
     * This setter method should only be used by unit tests
     *
     * @param service QcEgmsBillingGroupsService instance
     */
    protected void setQcEgmsBillingGroupsService(QcEgmsBillingGroupsService service) {
        this.qcEgmsBillingGroupsService = service;
    }

}