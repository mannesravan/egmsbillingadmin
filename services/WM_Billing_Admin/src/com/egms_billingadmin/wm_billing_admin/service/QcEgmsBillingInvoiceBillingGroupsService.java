/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingInvoiceBillingGroups;
import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingInvoiceCardprogramMap;
import com.egms_billingadmin.wm_billing_admin.QcEgmsBillingInvoiceConfigValues;

/**
 * Service object for domain model class {@link QcEgmsBillingInvoiceBillingGroups}.
 */
public interface QcEgmsBillingInvoiceBillingGroupsService {

    /**
     * Creates a new QcEgmsBillingInvoiceBillingGroups. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsBillingInvoiceBillingGroups if any.
     *
     * @param qcEgmsBillingInvoiceBillingGroups Details of the QcEgmsBillingInvoiceBillingGroups to be created; value cannot be null.
     * @return The newly created QcEgmsBillingInvoiceBillingGroups.
     */
    QcEgmsBillingInvoiceBillingGroups create(@Valid QcEgmsBillingInvoiceBillingGroups qcEgmsBillingInvoiceBillingGroups);


	/**
     * Returns QcEgmsBillingInvoiceBillingGroups by given id if exists.
     *
     * @param qcegmsbillinginvoicebillinggroupsId The id of the QcEgmsBillingInvoiceBillingGroups to get; value cannot be null.
     * @return QcEgmsBillingInvoiceBillingGroups associated with the given qcegmsbillinginvoicebillinggroupsId.
	 * @throws EntityNotFoundException If no QcEgmsBillingInvoiceBillingGroups is found.
     */
    QcEgmsBillingInvoiceBillingGroups getById(Long qcegmsbillinginvoicebillinggroupsId);

    /**
     * Find and return the QcEgmsBillingInvoiceBillingGroups by given id if exists, returns null otherwise.
     *
     * @param qcegmsbillinginvoicebillinggroupsId The id of the QcEgmsBillingInvoiceBillingGroups to get; value cannot be null.
     * @return QcEgmsBillingInvoiceBillingGroups associated with the given qcegmsbillinginvoicebillinggroupsId.
     */
    QcEgmsBillingInvoiceBillingGroups findById(Long qcegmsbillinginvoicebillinggroupsId);

	/**
     * Find and return the list of QcEgmsBillingInvoiceBillingGroups by given id's.
     *
     * If orderedReturn true, the return List is ordered and positional relative to the incoming ids.
     *
     * In case of unknown entities:
     *
     * If enabled, A null is inserted into the List at the proper position(s).
     * If disabled, the nulls are not put into the return List.
     *
     * @param qcegmsbillinginvoicebillinggroupsIds The id's of the QcEgmsBillingInvoiceBillingGroups to get; value cannot be null.
     * @param orderedReturn Should the return List be ordered and positional in relation to the incoming ids?
     * @return QcEgmsBillingInvoiceBillingGroups associated with the given qcegmsbillinginvoicebillinggroupsIds.
     */
    List<QcEgmsBillingInvoiceBillingGroups> findByMultipleIds(List<Long> qcegmsbillinginvoicebillinggroupsIds, boolean orderedReturn);


    /**
     * Updates the details of an existing QcEgmsBillingInvoiceBillingGroups. It replaces all fields of the existing QcEgmsBillingInvoiceBillingGroups with the given qcEgmsBillingInvoiceBillingGroups.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsBillingInvoiceBillingGroups if any.
     *
     * @param qcEgmsBillingInvoiceBillingGroups The details of the QcEgmsBillingInvoiceBillingGroups to be updated; value cannot be null.
     * @return The updated QcEgmsBillingInvoiceBillingGroups.
     * @throws EntityNotFoundException if no QcEgmsBillingInvoiceBillingGroups is found with given input.
     */
    QcEgmsBillingInvoiceBillingGroups update(@Valid QcEgmsBillingInvoiceBillingGroups qcEgmsBillingInvoiceBillingGroups);


    /**
     * Partially updates the details of an existing QcEgmsBillingInvoiceBillingGroups. It updates only the
     * fields of the existing QcEgmsBillingInvoiceBillingGroups which are passed in the qcEgmsBillingInvoiceBillingGroupsPatch.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsBillingInvoiceBillingGroups if any.
     *
     * @param qcegmsbillinginvoicebillinggroupsId The id of the QcEgmsBillingInvoiceBillingGroups to be deleted; value cannot be null.
     * @param qcEgmsBillingInvoiceBillingGroupsPatch The partial data of QcEgmsBillingInvoiceBillingGroups which is supposed to be updated; value cannot be null.
     * @return The updated QcEgmsBillingInvoiceBillingGroups.
     * @throws EntityNotFoundException if no QcEgmsBillingInvoiceBillingGroups is found with given input.
     */
    QcEgmsBillingInvoiceBillingGroups partialUpdate(Long qcegmsbillinginvoicebillinggroupsId, Map<String, Object> qcEgmsBillingInvoiceBillingGroupsPatch);

    /**
     * Deletes an existing QcEgmsBillingInvoiceBillingGroups with the given id.
     *
     * @param qcegmsbillinginvoicebillinggroupsId The id of the QcEgmsBillingInvoiceBillingGroups to be deleted; value cannot be null.
     * @return The deleted QcEgmsBillingInvoiceBillingGroups.
     * @throws EntityNotFoundException if no QcEgmsBillingInvoiceBillingGroups found with the given id.
     */
    QcEgmsBillingInvoiceBillingGroups delete(Long qcegmsbillinginvoicebillinggroupsId);

    /**
     * Deletes an existing QcEgmsBillingInvoiceBillingGroups with the given object.
     *
     * @param qcEgmsBillingInvoiceBillingGroups The instance of the QcEgmsBillingInvoiceBillingGroups to be deleted; value cannot be null.
     */
    void delete(QcEgmsBillingInvoiceBillingGroups qcEgmsBillingInvoiceBillingGroups);

    /**
     * Find all QcEgmsBillingInvoiceBillingGroups matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
     *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
     *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching QcEgmsBillingInvoiceBillingGroups.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
     */
    @Deprecated
    Page<QcEgmsBillingInvoiceBillingGroups> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
     * Find all QcEgmsBillingInvoiceBillingGroups matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching QcEgmsBillingInvoiceBillingGroups.
     *
     * @see Pageable
     * @see Page
     */
    Page<QcEgmsBillingInvoiceBillingGroups> findAll(String query, Pageable pageable);

    /**
     * Exports all QcEgmsBillingInvoiceBillingGroups matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
     */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

    /**
     * Exports all QcEgmsBillingInvoiceBillingGroups matching the given input query to the given exportType format.
     *
     * @param options The export options provided by the user; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @param outputStream The output stream of the file for the exported data to be written to.
     *
     * @see DataExportOptions
     * @see Pageable
     * @see OutputStream
     */
    void export(DataExportOptions options, Pageable pageable, OutputStream outputStream);

    /**
     * Retrieve the count of the QcEgmsBillingInvoiceBillingGroups in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
     * @return The count of the QcEgmsBillingInvoiceBillingGroups.
     */
    long count(String query);

    /**
     * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return Paginated data with included fields.
     *
     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
    Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);

    /*
     * Returns the associated qcEgmsBillingInvoiceCardprogramMaps for given QcEgmsBillingInvoiceBillingGroups id.
     *
     * @param invoicebillinggroupid value of invoicebillinggroupid; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated QcEgmsBillingInvoiceCardprogramMap instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<QcEgmsBillingInvoiceCardprogramMap> findAssociatedQcEgmsBillingInvoiceCardprogramMaps(Long invoicebillinggroupid, Pageable pageable);

    /*
     * Returns the associated qcEgmsBillingInvoiceConfigValueses for given QcEgmsBillingInvoiceBillingGroups id.
     *
     * @param invoicebillinggroupid value of invoicebillinggroupid; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated QcEgmsBillingInvoiceConfigValues instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<QcEgmsBillingInvoiceConfigValues> findAssociatedQcEgmsBillingInvoiceConfigValueses(Long invoicebillinggroupid, Pageable pageable);

}