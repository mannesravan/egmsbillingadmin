/*Copyright (c) 2019-2020 qwikcilver.com All Rights Reserved.
 This software is the confidential and proprietary information of qwikcilver.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with qwikcilver.com*/
package com.egms_billingadmin.wm_billing_admin.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.DataExportOptions;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.egms_billingadmin.wm_billing_admin.QcEgmsCardProgramGroups;
import com.egms_billingadmin.wm_billing_admin.QcEgmsCardprogramgroupStatusLk;

/**
 * Service object for domain model class {@link QcEgmsCardprogramgroupStatusLk}.
 */
public interface QcEgmsCardprogramgroupStatusLkService {

    /**
     * Creates a new QcEgmsCardprogramgroupStatusLk. It does cascade insert for all the children in a single transaction.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsCardprogramgroupStatusLk if any.
     *
     * @param qcEgmsCardprogramgroupStatusLk Details of the QcEgmsCardprogramgroupStatusLk to be created; value cannot be null.
     * @return The newly created QcEgmsCardprogramgroupStatusLk.
     */
    QcEgmsCardprogramgroupStatusLk create(@Valid QcEgmsCardprogramgroupStatusLk qcEgmsCardprogramgroupStatusLk);


	/**
     * Returns QcEgmsCardprogramgroupStatusLk by given id if exists.
     *
     * @param qcegmscardprogramgroupstatuslkId The id of the QcEgmsCardprogramgroupStatusLk to get; value cannot be null.
     * @return QcEgmsCardprogramgroupStatusLk associated with the given qcegmscardprogramgroupstatuslkId.
	 * @throws EntityNotFoundException If no QcEgmsCardprogramgroupStatusLk is found.
     */
    QcEgmsCardprogramgroupStatusLk getById(Long qcegmscardprogramgroupstatuslkId);

    /**
     * Find and return the QcEgmsCardprogramgroupStatusLk by given id if exists, returns null otherwise.
     *
     * @param qcegmscardprogramgroupstatuslkId The id of the QcEgmsCardprogramgroupStatusLk to get; value cannot be null.
     * @return QcEgmsCardprogramgroupStatusLk associated with the given qcegmscardprogramgroupstatuslkId.
     */
    QcEgmsCardprogramgroupStatusLk findById(Long qcegmscardprogramgroupstatuslkId);

	/**
     * Find and return the list of QcEgmsCardprogramgroupStatusLks by given id's.
     *
     * If orderedReturn true, the return List is ordered and positional relative to the incoming ids.
     *
     * In case of unknown entities:
     *
     * If enabled, A null is inserted into the List at the proper position(s).
     * If disabled, the nulls are not put into the return List.
     *
     * @param qcegmscardprogramgroupstatuslkIds The id's of the QcEgmsCardprogramgroupStatusLk to get; value cannot be null.
     * @param orderedReturn Should the return List be ordered and positional in relation to the incoming ids?
     * @return QcEgmsCardprogramgroupStatusLks associated with the given qcegmscardprogramgroupstatuslkIds.
     */
    List<QcEgmsCardprogramgroupStatusLk> findByMultipleIds(List<Long> qcegmscardprogramgroupstatuslkIds, boolean orderedReturn);


    /**
     * Updates the details of an existing QcEgmsCardprogramgroupStatusLk. It replaces all fields of the existing QcEgmsCardprogramgroupStatusLk with the given qcEgmsCardprogramgroupStatusLk.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsCardprogramgroupStatusLk if any.
     *
     * @param qcEgmsCardprogramgroupStatusLk The details of the QcEgmsCardprogramgroupStatusLk to be updated; value cannot be null.
     * @return The updated QcEgmsCardprogramgroupStatusLk.
     * @throws EntityNotFoundException if no QcEgmsCardprogramgroupStatusLk is found with given input.
     */
    QcEgmsCardprogramgroupStatusLk update(@Valid QcEgmsCardprogramgroupStatusLk qcEgmsCardprogramgroupStatusLk);


    /**
     * Partially updates the details of an existing QcEgmsCardprogramgroupStatusLk. It updates only the
     * fields of the existing QcEgmsCardprogramgroupStatusLk which are passed in the qcEgmsCardprogramgroupStatusLkPatch.
     *
     * This method overrides the input field values using Server side or database managed properties defined on QcEgmsCardprogramgroupStatusLk if any.
     *
     * @param qcegmscardprogramgroupstatuslkId The id of the QcEgmsCardprogramgroupStatusLk to be deleted; value cannot be null.
     * @param qcEgmsCardprogramgroupStatusLkPatch The partial data of QcEgmsCardprogramgroupStatusLk which is supposed to be updated; value cannot be null.
     * @return The updated QcEgmsCardprogramgroupStatusLk.
     * @throws EntityNotFoundException if no QcEgmsCardprogramgroupStatusLk is found with given input.
     */
    QcEgmsCardprogramgroupStatusLk partialUpdate(Long qcegmscardprogramgroupstatuslkId, Map<String, Object> qcEgmsCardprogramgroupStatusLkPatch);

    /**
     * Deletes an existing QcEgmsCardprogramgroupStatusLk with the given id.
     *
     * @param qcegmscardprogramgroupstatuslkId The id of the QcEgmsCardprogramgroupStatusLk to be deleted; value cannot be null.
     * @return The deleted QcEgmsCardprogramgroupStatusLk.
     * @throws EntityNotFoundException if no QcEgmsCardprogramgroupStatusLk found with the given id.
     */
    QcEgmsCardprogramgroupStatusLk delete(Long qcegmscardprogramgroupstatuslkId);

    /**
     * Deletes an existing QcEgmsCardprogramgroupStatusLk with the given object.
     *
     * @param qcEgmsCardprogramgroupStatusLk The instance of the QcEgmsCardprogramgroupStatusLk to be deleted; value cannot be null.
     */
    void delete(QcEgmsCardprogramgroupStatusLk qcEgmsCardprogramgroupStatusLk);

    /**
     * Find all QcEgmsCardprogramgroupStatusLks matching the given QueryFilter(s).
     * All the QueryFilter(s) are ANDed to filter the results.
     * This method returns Paginated results.
     *
     * @deprecated Use {@link #findAll(String, Pageable)} instead.
     *
     * @param queryFilters Array of queryFilters to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching QcEgmsCardprogramgroupStatusLks.
     *
     * @see QueryFilter
     * @see Pageable
     * @see Page
     */
    @Deprecated
    Page<QcEgmsCardprogramgroupStatusLk> findAll(QueryFilter[] queryFilters, Pageable pageable);

    /**
     * Find all QcEgmsCardprogramgroupStatusLks matching the given input query. This method returns Paginated results.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of matching QcEgmsCardprogramgroupStatusLks.
     *
     * @see Pageable
     * @see Page
     */
    Page<QcEgmsCardprogramgroupStatusLk> findAll(String query, Pageable pageable);

    /**
     * Exports all QcEgmsCardprogramgroupStatusLks matching the given input query to the given exportType format.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param exportType The format in which to export the data; value cannot be null.
     * @param query The query to filter the results; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return The Downloadable file in given export type.
     *
     * @see Pageable
     * @see ExportType
     * @see Downloadable
     */
    Downloadable export(ExportType exportType, String query, Pageable pageable);

    /**
     * Exports all QcEgmsCardprogramgroupStatusLks matching the given input query to the given exportType format.
     *
     * @param options The export options provided by the user; No filters applied if the input is null/empty.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @param outputStream The output stream of the file for the exported data to be written to.
     *
     * @see DataExportOptions
     * @see Pageable
     * @see OutputStream
     */
    void export(DataExportOptions options, Pageable pageable, OutputStream outputStream);

    /**
     * Retrieve the count of the QcEgmsCardprogramgroupStatusLks in the repository with matching query.
     * Note: Go through the documentation for <u>query</u> syntax.
     *
     * @param query query to filter results. No filters applied if the input is null/empty.
     * @return The count of the QcEgmsCardprogramgroupStatusLk.
     */
    long count(String query);

    /**
     * Retrieve aggregated values with matching aggregation info.
     *
     * @param aggregationInfo info related to aggregations.
     * @param pageable Details of the pagination information along with the sorting options. If null exports all matching records.
     * @return Paginated data with included fields.
     *
     * @see AggregationInfo
     * @see Pageable
     * @see Page
	 */
    Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable);

    /*
     * Returns the associated qcEgmsCardProgramGroupses for given QcEgmsCardprogramgroupStatusLk id.
     *
     * @param cardprogramgroupstatusid value of cardprogramgroupstatusid; value cannot be null
     * @param pageable Details of the pagination information along with the sorting options. If null returns all matching records.
     * @return Paginated list of associated QcEgmsCardProgramGroups instances.
     *
     * @see Pageable
     * @see Page
     */
    Page<QcEgmsCardProgramGroups> findAssociatedQcEgmsCardProgramGroupses(Long cardprogramgroupstatusid, Pageable pageable);

}